package model;

import java.util.ArrayList;
import java.util.List;

import sem.SemanticsManager;

import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.vocabulary.RDF;

import eval.GeoSemantics;

/**
 * Class that gathers some features extracted
 * from the RDF graph formed by the entities given
 * in input and their neighborhood.
 *
 */
public class SemFeatures {
	
	private static final Integer MAX_ENT = 10;
	// Paris: > 40.000, California: > 80.000
	private static final Integer MAX_NGB = 15000;

	private Integer p, png, n, e, ng, cng;
	private Double gc, a;
	
	public SemFeatures(List<Resource> entities) throws ModelException {
		if (entities.size() > MAX_ENT) {
			throw new ModelException("Too many entities"); // too much resource consuming
		}
		cng = 0; png = 0; e = 0;
		a = Double.MAX_VALUE; gc = 0.;
		List<String> mergedNeighborhood = new ArrayList<>();
		List<Place> places = new ArrayList<>();
		for (Resource ent : entities) {
			if (Place.isAPlace(ent)) {
				Place p;
				try {
					p = new Place(ent);
					if (p.getArea() != null && p.getArea() < a) {
						a = p.getArea();
					}
					if (p.getCoords() != null) {
						// TODO not really useful. Check the inclusion instead
						// (introduce Place Ontology)
						for (Place otherP : places) {
							Double dist = GeoCoords.dist(p.getCoords(), otherP.getCoords());
							if (dist != null) {
								gc += dist;
							}
						}
					}
					places.add(p);
				} catch (ModelException e1) {
					// should not happen
				}
			}
			List<RDFNode> neighborhood = SemanticsManager.getNeighbors(ent);
			if (neighborhood.size() > MAX_NGB) {
				throw new ModelException("Too large neighborhood"); // too much resource-consuming
			}
			for (Resource otherEnt : entities) {
				if (neighborhood.contains(otherEnt)) {
					e++;
				}
			}
			for (RDFNode node : neighborhood) {
				if (!mergedNeighborhood.contains(node.toString())) {
					mergedNeighborhood.add(node.toString());
					if (node.isResource() && Place.isAPlace((Resource) node)) {
						png++;
					}
				} else {
					// no need to remember which one, each duplicate counts 1
					cng++;
				}
			}
		}
		n = entities.size();
		ng = mergedNeighborhood.size();
		p = places.size();
		e /= 2; // each edge is computed 2 times
	}
	
	public Integer getNbOfPlaces() {
		return p;
	}
	
	public Integer getNbOfPlacesNeighborhood() {
		return png;
	}
	
	public Integer getNbOfEntities() {
		return n;
	}
	
	public Integer getNbOfLinkedEntities() {
		return e;
	}
	
	public Integer getNbOfEntitiesNeighborhood() {
		return ng;
	}
	
	public Integer getNbOfCommonEntitiesNeighborhood() {
		return cng;
	}
	
	public Double getGeographicalCoherency() {
		return gc;
	}
	
	public Double getSmallestArea() {
		return a;
	}
	
	@Override
	public String toString() {
		return "(" + p + "," + png + "," + n + "," + e + "," + ng + "," + cng + "," + gc + "," + a + ")";
	}
	
}
