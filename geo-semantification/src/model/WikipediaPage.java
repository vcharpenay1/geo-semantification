package model;

import java.io.IOException;
import java.net.MalformedURLException;
import java.text.BreakIterator;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import db.WikipediaStopWordService;

public class WikipediaPage {
	
	private String mUrl;
	private Map<String, Integer> mWords;
	
	/**
	 * Tries to retrieve the Wikipedia document given in parameter
	 * @param url
	 * @throws MalformedURLException
	 */
	public WikipediaPage(String url) throws MalformedURLException {
		try {
			Document doc = Jsoup.connect(url).get();
			mUrl = url;
//			Long before = System.currentTimeMillis();
			mWords = getWordsFromDoc(doc);
//			Long after = System.currentTimeMillis();
//			System.out.println("parsed page in " + (after - before) + " ms");
		} catch (IOException e) {
			throw new MalformedURLException("The given page does not exist, is unaccessible or unavailable...");
		}
	}
	
	public String getUrl() {
		return mUrl;
	}
	
	public Map<String, Integer> getWords() {
		return mWords;
	}
	
	/**
	 * Splits the text contained in the Wikipedia page and counts
	 * the frequency of all words.
	 * @return a list of all keywords in the corresponding
	 * Wikipedia page with their term frequency
	 */
	private Map<String, Integer> getWordsFromDoc(Document doc) {
		Element content = doc.select("#mw-content-text").get(0); // returns only one element
		for (Element e : content.select(".navbox, .metadata, .hatnote")) {
			// removes content that is not directly related to the article
			e.remove();
		}
		String text = content.text();
		Map<String, Integer> words = new HashMap<>();
		BreakIterator it = BreakIterator.getWordInstance(new Locale("en-us")); // TODO locale as a parameter
		it.setText(text);
		int start = it.first();
		for (int end = it.next(); end != BreakIterator.DONE; start = end, end = it.next()) {
			// string is normalized (lower-cased, without dash)
			String w = text.substring(start,end).toLowerCase().replace("-", "");
			if (w.matches("[\\p{IsLatin}]+") && !WikipediaStopWordService.isAStopWord(w)) {
				 // regex matches words with any letter from UTF latin script
				// if no filtering, punctuation marks are also considered as tokens
				if (words.containsKey(w)) {
					Integer f = words.get(w);
					words.put(w, f + 1);
				} else {
					words.put(w, 1);
				}
			}
	    }
		return words;
	}
	
}
