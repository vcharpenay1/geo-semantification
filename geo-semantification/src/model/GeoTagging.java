package model;

public class GeoTagging {
	
	private Image mIm;
	private Place mLocation;
	
	public GeoTagging(Image im, Place p) {
		mIm = im;
		mLocation = p;
	}
	
	public Image getImage() {
		return mIm;
	}
	
	public Place getLocation() {
		return mLocation;
	}

}
