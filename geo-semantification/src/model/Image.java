package model;

import java.net.URL;
import java.sql.Date;
import java.util.List;


public class Image {

	private Long mId;
	private String mTitle;
	private List<String> mTags;
	private GeoCoords mCoords;
	private String mUserId;
	private Date mDateTaken;
	private URL mUrl;
	
	public Image() {
		// do nothing
	}

	public Image(Long id, String title, List<String> tags, GeoCoords coords,
			String userId, Date dateTaken, URL url) {
		super();
		this.mId = id;
		this.mTitle = title;
		this.mTags = tags;
		this.mCoords = coords;
		this.mUserId = userId;
		this.mDateTaken = dateTaken;
		this.mUrl = url;
	}

	public Long getId() {
		return mId;
	}

	public void setId(Long id) {
		this.mId = id;
	}

	public String getTitle() {
		return mTitle;
	}

	public void setTitle(String title) {
		this.mTitle = title;
	}

	public List<String> getTags() {
		return mTags;
	}

	public void setTags(List<String> tags) {
		this.mTags = tags;
	}

	public GeoCoords getCoords() {
		return mCoords;
	}

	public void setCoords(GeoCoords coords) {
		this.mCoords = coords;
	}

	public String getUserId() {
		return mUserId;
	}

	public void setUserId(String userId) {
		this.mUserId = userId;
	}

	public Date getDateTaken() {
		return mDateTaken;
	}

	public void setDateTaken(Date dateTaken) {
		this.mDateTaken = dateTaken;
	}

	public URL getUrl() {
		return mUrl;
	}

	public void setUrl(URL url) {
		this.mUrl = url;
	}
	
}
