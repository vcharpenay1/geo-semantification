package model;

import java.util.ArrayList;
import java.util.List;

import sem.SemanticsManager;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Resource;

/**
 * Warning: this class wraps both Places as defined
 * by Wikipedia and other entities that contains
 * geographical information, even if they are no
 * proper landmarks
 *
 */
public class Place {
	
	private static final Resource PLACE;
	/**
	 * A flat, simplified version of DBpedia Place ontology
	 */
	private static List<Resource> mOnto = new ArrayList<>();

	static {
		Model m = ModelFactory.createDefaultModel();
		
		PLACE = m.createResource("http://dbpedia.org/ontology/Place");
		
		mOnto.add(m.createResource("http://dbpedia.org/ontology/ArchitecturalStructure"));
		mOnto.add(m.createResource("http://dbpedia.org/ontology/NaturalPlace"));
		mOnto.add(m.createResource("http://dbpedia.org/ontology/SportFacility"));
		mOnto.add(m.createResource("http://dbpedia.org/ontology/Country"));
		mOnto.add(m.createResource("http://dbpedia.org/ontology/Region"));
		mOnto.add(m.createResource("http://dbpedia.org/ontology/Settlement"));
	}
	
	private GeoCoords mCoords;
	private Resource mEntity;
	private Double mArea;
	
	public Place(Resource e) throws ModelException {
		if (isAPlace(e)) {
			mCoords = SemanticsManager.getLocation(e);
			mArea = SemanticsManager.getArea(e);
			mEntity = e;
		} else {
			throw new ModelException("The given entity is not of type Place or does not have coordinates");
		}
	}
	
	public GeoCoords getCoords() {
		return mCoords;
	}
	
	public void setCoords(GeoCoords coords) {
		this.mCoords = coords;
	}
	
	public Resource getEntity() {
		return mEntity;
	}
	
	public void setEntity(Resource e) {
		mEntity = e;
	}
	
	public Resource getType() {
		Resource def = PLACE; // default type
		List<Resource> types = SemanticsManager.getEntityTypes(mEntity);
		for (Resource type : types) {
			if (mOnto.contains(type)) {
				return type;
			}
		}
		return def;
	}
	
	public Double getArea() {
		return mArea;
	}
	
	public void setArea(Double area) {
		this.mArea = area;
	}
	
	public static boolean isAPlace(Resource entity) {
		List<Resource> types = null;
		if (entity != null) {
			return (SemanticsManager.getLocation(entity) != null)
					|| ((types = SemanticsManager.getEntityTypes(entity)) != null && types.contains(PLACE));
		}
		return false;
	}

}
