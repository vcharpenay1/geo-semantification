package model;

public class GeoCoords {
	
	private double mLon;
	private double mLat;
	
	public GeoCoords(double mLon, double mLat) {
		super();
		this.mLon = mLon;
		this.mLat = mLat;
	}
	
	@Override
	public String toString() {
		return "(" + mLon + ", " + mLat + ")";
	}
	
	/**
	 * Returns the distance between 2 points along the surface of the earth
	 * (47 CFR 73.208 prescription of the FCC)
	 * @param c1
	 * @param c2
	 * @return
	 */
	public static Double dist(GeoCoords c1, GeoCoords c2) {
		if (c1 == null || c2 == null) {
			return null;
		}
		Double deltaLat = Math.abs(c2.getLat() - c1.getLat());
		Double deltaLon = Math.abs(c2.getLon() - c1.getLon());
		Double meanLat = (c2.getLat() + c1.getLat()) / 2;
		Double k1 = 111.13209 - 0.56605 * Math.cos(2 * meanLat) + 0.0012 * Math.cos(4 * meanLat);
		Double k2 = 111.41513 * Math.cos(meanLat) - 0.09455 * Math.cos(3 * meanLat) + 0.00012 * Math.cos(5 * meanLat);
		
		return Math.sqrt(Math.pow(k1 * deltaLat, 2) + Math.pow(k2 * deltaLon, 2));
	}

	public double getLon() {
		return mLon;
	}

	public void setLon(double lon) throws ModelException {
		if (isLon(lon)) {
			this.mLon = lon;
		} else {
			throw new ModelException("The given longitude is not valid");
		}
	}

	public double getLat() {
		return mLat;
	}

	public void setLat(double lat) throws ModelException {
		if (isLat(lat)) {
			this.mLat = lat;
		} else {
			throw new ModelException("The given latitude is not valid");
		}
	}
	
	private Boolean isLon(double lon) {
		return lon >= -180. && lon <= 180.;
	}
	
	private Boolean isLat(double lat) {
		return lat >= -90. && lat <= 90.;
	}
	
}
