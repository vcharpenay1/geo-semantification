package ir;

public class RetrievalUtil {
	
	public static void tfIdf() {
		// TODO
	}
	
	public static double cosSimilarity(double[] v1, double[] v2) {
		if (v1.length != v2.length) {
			return 0; // should return NaN, but this value is considered as higher than any other double...
		}
		double dotProduct = 0.;
		double v1Norm = 0.;
		double v2Norm = 0.;
		for (int i = 0; i < v1.length; i++) {
			dotProduct += v1[i] * v2[i];
			v1Norm += v1[i] * v1[i];
			v2Norm += v2[i] * v2[i];
		}
		v1Norm = Math.sqrt(v1Norm);
		v2Norm = Math.sqrt(v2Norm);
		if (v1Norm == 0. || v2Norm == 0) {
			return 0;
		}
		return dotProduct / (v1Norm * v2Norm);
	}

}
