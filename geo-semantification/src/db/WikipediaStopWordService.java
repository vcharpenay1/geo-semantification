package db;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class WikipediaStopWordService {
	
	private static Set<String> mStopWords = null;
	
	/**
	 * reads the given file and store its content as a
	 * list of stop-words
	 * @param file a standard CVS file
	 * @throws IOException
	 */
	public static void init(String filename) throws IOException {
		mStopWords = new HashSet<>();
		File f = new File(filename);
		BufferedReader reader = new BufferedReader(new FileReader(f));
		String line;
		while ((line = reader.readLine()) != null) {
			for (String sw : line.split(",")) {
				mStopWords.add(sw);
			}
		}
		reader.close();
	}
	
	/**
	 * checks if the word contains one root from the list.
	 * If the list was not initialized by <code>init()</code>,
	 * consider no word as a stop-word
	 * @param tag
	 * @return
	 */
	public static boolean isAStopWord(String tag) {
		if (mStopWords == null) {
			return false;
		}
		return mStopWords.contains(tag);
	}

}
