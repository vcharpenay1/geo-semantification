package db;

import java.sql.SQLException;

/**
 * Provides a global access to DB managers with only one initialization
 * @author victor
 *
 */
public class DBConnector {

	private static WikipediaDBManager mW;
	private static FlickrDBManager mF;
	private static EvalDBManager mE;
	
	public static void initWikipediaDB(String host, String port, String dbName, String username, String password) throws SQLException {
		mW = new WikipediaDBManager(host, port, dbName, username, password);
	}
	
	public static void initFlickrDB(String host, String port, String dbName, String username, String password) throws SQLException {
		mF = new FlickrDBManager(host, port, dbName, username, password);
	}
	
	public static void initEvalDB(String dbFile) throws SQLException {
		mE = new EvalDBManager(dbFile);
	}
	
	public static WikipediaDBManager getWikipediaDB() {
		return mW;
	}
	
	public static FlickrDBManager getFlickrDB() {
		return mF;
	}
	
	public static EvalDBManager getEvalDB() {
		return mE;
	}
	
}
