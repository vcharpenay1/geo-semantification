package db;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.BatchUpdateException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import org.postgresql.util.PSQLException;
import org.xml.sax.ext.LexicalHandler;

import edu.cmu.lti.jawjaw.db.SenseDAO;
import edu.cmu.lti.jawjaw.db.SynlinkDAO;
import edu.cmu.lti.jawjaw.pobj.Lang;
import edu.cmu.lti.jawjaw.pobj.Link;
import edu.cmu.lti.jawjaw.pobj.POS;
import edu.cmu.lti.jawjaw.pobj.Sense;
import edu.cmu.lti.jawjaw.pobj.Synlink;
import edu.cmu.lti.jawjaw.pobj.Synset;
import edu.cmu.lti.jawjaw.util.WordNetUtil;
import edu.cmu.lti.lexical_db.NictWordNet;
import model.GeoCoords;
import model.Image;
import model.WikipediaPage;

public class WikipediaDBManager extends AbstractPostgresDBManager {

	private static final String INSERT_PAGE = "insert into index (tag, title) values (?, ?)";
	private static final String UPDATE_DIS_PAGE = "update index set tag = ? where title = ?";
	private static final String GET_DIS_PAGE = "select title from index where title like '%(%)%' or title like '%,%'";
	private static final String GET_PAGES = "select * from index where tag like ?";
	private static final String GET_PAGES_EX = "select * from index where tag = ?";
	
	private NictWordNet mWN;
	
	public WikipediaDBManager(String host, String port, String dbName,
			String username, String password) throws SQLException {
		super(host, port, dbName, username, password);
		mWN = new NictWordNet();
	}

	public void createIndex(File f) throws SQLException {
		try {
			BufferedReader reader = new BufferedReader(new FileReader(f));
			String line;
			PreparedStatement s = mConnection.prepareStatement(INSERT_PAGE);
			while ((line = reader.readLine()) != null) {
				for (String v : genVariations(line)) {
					if (mConnection != null) {
						if (!v.isEmpty()) {
							s.setString(1, v); // tag
							s.setString(2, line); // title
							try {
								s.execute();
							} catch (PSQLException e) {
								System.out.println("No index created for: " + line + " (" + e.getMessage() + ")");
							}
						}
					} else {
						throw new SQLException("Not connected to the database. Abort query.");
					}
				}
			}
			reader.close();
		} catch (IOException e) {
			System.out.println("Error while reading Wikipedia dump file. Abort query.");
		}
	}
	
	/**
	 * Updates the index by transforming all tags related to titles
	 * that follow the Title Disambiguation Guidelines from Wikipedia
	 * (i.e. containing parentheses or commas).
	 * @throws SQLException
	 */
	public void processDisambiguation() throws SQLException {
		PreparedStatement sSelect = mConnection.prepareStatement(GET_DIS_PAGE);
		PreparedStatement sUpdate = mConnection.prepareStatement(UPDATE_DIS_PAGE);

		ResultSet res = sSelect.executeQuery();
		while (res.next()) {
			String title = res.getString("title");
			if (!isDisPage(title)) {
				// TODO process disambiguation pages by indexing the pointer they contain...
				List<String> tags = genVariations(title.split("[,(]")[0]);
				for (String tag : tags) {
					if (mConnection != null) {
						try {
							sUpdate.setString(1, tag);
							sUpdate.setString(2, title);
							sUpdate.execute();
						} catch (PSQLException e) {
							System.out.println("No modification for title: " + title + " (" + e.getMessage() + ")");
						}
					} else {
						throw new SQLException("Not connected to the database. Abort query.");
					}
				}
			}
		}
	}

	/**
	 * Returns all the pages which title contains the given tag
	 * (as one or more whole words in the title).
	 * Pages's titles are then filtered: if some contain non-proper
	 * nouns which are not in the given tag, they are not likely
	 * related to the tag...
	 * If exactMatch argument is given, only retrieves the pages
	 * which title equals the tag.
	 * @param tag
	 * @param exactMatch if true, only retrieves the pages that
	 * have a title that equals tag
	 * @return
	 * @throws SQLException
	 */
	public List<WikipediaPage> getPages(String tag, Boolean exactMatch) throws SQLException {
		if (mConnection != null) {
			PreparedStatement s;
			if (exactMatch) {
				s = mConnection.prepareStatement(GET_PAGES_EX);
				s.setString(1, tag);
			} else {
				s = mConnection.prepareStatement(GET_PAGES);
				s.setString(1, "%" + tag + "%");
			}
			ResultSet res = s.executeQuery();
			List<WikipediaPage> pages = new ArrayList<>();
			while (res.next()) {
				String t = res.getString("title");
				if (exactMatch || (containsWhole(t, tag) && !containsNoun(t, tag) && !isDisPage(t))) {
					// no filtering is done if exact matches are retrieved
					try {
						pages.add(new WikipediaPage(nameToURL(t)));
					} catch (MalformedURLException e) {
						System.err.println("Wikipedia page could not be built from title '" + t + "'. The page may be deleted.");
					}
				}
			}
			return pages;
		} else {
			throw new SQLException("Not connected to the database. Abort query.");
		}
	}
	
	/**
	 * Returns whether the given tag has more than one entry
	 * in the index, or null if the tag does not exist.
	 * @param tag
	 * @return
	 * @throws SQLException 
	 */
	public Boolean isAmbiguous(String tag) throws SQLException {
		if (mConnection != null) {
			PreparedStatement s = mConnection.prepareStatement(GET_PAGES_EX);
			s.setString(1, tag);
			ResultSet res = s.executeQuery();
			if (res.next()) {
				return res.next();
			} else {
				return null;
			}
		} else {
			throw new SQLException("Not connected to the database. Abort query.");
		}
	}
	
	/**
	 * Generates possible variations of the name of a given
	 * Wikipedia page. For now, only normalizes the original
	 * TODO try variants when (_) appears in page's name
	 * name
	 * @param original
	 * @return
	 */
	private List<String> genVariations(String original) {
		List<String> vars = new ArrayList<>();
//		if (original.contains("_")) {
//			String[] ws = original.split("_");
//			for (String w : ws) {
//				vars.add(filter(original));
//			}
//		}
		vars.add(filter(original));
		return vars;
	}
	
	/**
	 * Removes all non-Latin characters, digits or '.'
	 * from the original string s
	 * @param s
	 * @return
	 */
	private String filter(String s) {
		return s.replaceAll("[^\\p{IsLatin}0-9.]*", "").toLowerCase();
	}
	
	/**
	 * Checks that the given title (where words are separated with '_')
	 * contains tag as one or more whole words, e.g:
	 * (Berlin_Airport, berlin) -> true
	 * (Berline, berlin) -> false
	 * (Berlin's_Wall) -> false
	 * (San_Fransisco, sanfransisco) -> true
	 * @param title
	 * @param tag
	 * @return
	 */
	private Boolean containsWhole(String title, String tag) {
		String[] words = title.split("_");
		int n = 1; // n / n-tuples by choosing n words, ordered, from 'words'
		while (n <= words.length) {
			for (int i = 0; i < words.length - (n - 1); i++) {
				// forms a n-tuple of the words
				String nTuple = words[i];
				for (int p = 1; p < n; p++) {
					nTuple = nTuple.concat(words[i + p]);
				}
				if (tag.equals(nTuple.toLowerCase())) {
					return true;
				}
			}
			n++;
		}
		return false;
	}
	
	/**
	 * Returns if the given title contains a non-proper name
	 * that is not contained in the associated tag. If a name
	 * has several meanings, it returns true only if all meaning
	 * are related to a non-proper name. Lexical processing
	 * is done with WordNet.
	 * @param title
	 * @param tag
	 * @return
	 */
	private Boolean containsNoun(String title, String tag) {
		for (String w : title.split("_")) {
			w = w.toLowerCase(); // normalized
			if (!tag.contains(w)) {
				List<Synset> syns = WordNetUtil.wordToSynsets(w, POS.n);
				List<Synlink> links = new ArrayList<>();
				for (Synset s : syns) {
					// a synset represents proper names if it has an 'instance' relation
					 links.addAll(SynlinkDAO.findSynlinksBySynsetAndLink(s.getSynset(), Link.inst));
				}
				if (!syns.isEmpty() && links.isEmpty()) {
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * Returns whether the given page is a disambiguation page or not
	 * @param title
	 * @return
	 */
	private Boolean isDisPage(String title) {
		return title.contains("(disambiguation)");
	}
	
	private String nameToURL(String title) {
		return "http://en.wikipedia.org/wiki/" + title;
	}
	
}
