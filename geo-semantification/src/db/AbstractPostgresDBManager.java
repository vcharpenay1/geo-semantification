package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public abstract class AbstractPostgresDBManager {
	
	protected Connection mConnection;
	
	public AbstractPostgresDBManager(String host, String port, String dbName,
			String username, String password) throws SQLException {
		String url = "jdbc:postgresql://" + host + ":" + port + "/" + dbName;
		mConnection = DriverManager.getConnection(url, username, password);
	}
	
}
