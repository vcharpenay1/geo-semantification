package db;

import java.util.HashSet;
import java.util.Set;

public class FlickrStopWordService {
	
	private static final Set<String> STOP_WORDS;
	
	static {
		STOP_WORDS = new HashSet<>();
		STOP_WORDS.add("nikon");
		STOP_WORDS.add("canon");
		STOP_WORDS.add("iphonography");
		STOP_WORDS.add("instagram");
		STOP_WORDS.add("blackandwhite");
		STOP_WORDS.add("blackwhite");
		STOP_WORDS.add("bw"); // black and white again
		STOP_WORDS.add("noiretblanc");
		STOP_WORDS.add("blancoynegro");
		STOP_WORDS.add("foto");
		STOP_WORDS.add("hdr"); // high dynamic range
		STOP_WORDS.add("photo");
		STOP_WORDS.add("kodak");
		STOP_WORDS.add("squareformat"); // because of instagram
		STOP_WORDS.add("camera");
		STOP_WORDS.add("pentax");
		STOP_WORDS.add("cameraphone");
		STOP_WORDS.add("eos");
		STOP_WORDS.add("photomatrix");
		STOP_WORDS.add("flickr");
		STOP_WORDS.add("fujifilm");
		STOP_WORDS.add("sony");
		STOP_WORDS.add("leica");
		STOP_WORDS.add("longexposure");
		STOP_WORDS.add("geotagged");
		STOP_WORDS.add("iphone");
	}
	
	/**
	 * checks 3 things:
	 * - if the word contains one root from the list
	 * - if the word contains any digit
	 * - if the word is a machine tag (contains "*:*")
	 * @param tag
	 * @return
	 */
	public static boolean isAStopWord(String tag) {
		return STOP_WORDS.contains(tag) ||
			   tag.matches(".*[^A-Za-z].*") ||
			   tag.matches(".*:.*");
	}

}
