package db;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import model.GeoCoords;
import model.Image;

public class FlickrDBManager extends AbstractPostgresDBManager {
	
	private static final String GET_IMAGE = "select * from photos_new where id=?";
	private static final String GET_IMAGES = "select id from photos_new limit ?";
	private static final String GET_IMAGES_RAND = "select id from photos_new order by random() limit ?";
	private static final String GET_ALL_IMAGES = "select id from photos_new";
	private static final String GET_ALL_IMAGES_RAND = "select id from photos_new order by random()";
	private static final String GET_USERID = "select * from photos_new where id=?";
	
	public FlickrDBManager(String host, String port, String dbName,
			String username, String password) throws SQLException {
		super(host, port, dbName, username, password);
	}

	public List<Long> getImages(Long limit, Boolean random) throws SQLException {
		if (mConnection != null) {
			PreparedStatement s = mConnection.prepareStatement(random ? GET_IMAGES_RAND : GET_IMAGES);
			s.setLong(1, limit);
			ResultSet res = s.executeQuery();
			List<Long> ids = new ArrayList<>();
			while (res.next()) {
				ids.add(res.getLong("id"));
			}
			return ids;
		} else {
			throw new SQLException("Not connected to the database. Abort query.");
		}
	}

	public List<Long> getImages(Boolean random) throws SQLException {
		if (mConnection != null) {
			PreparedStatement s = mConnection.prepareStatement(random ? GET_ALL_IMAGES_RAND : GET_ALL_IMAGES);
			ResultSet res = s.executeQuery();
			List<Long> ids = new ArrayList<>();
			while (res.next()) {
				ids.add(res.getLong("id"));
			}
			return ids;
		} else {
			throw new SQLException("Not connected to the database. Abort query.");
		}
	}

	public Image getImage(Long id) throws SQLException {
		if (mConnection != null) {
			PreparedStatement s = mConnection.prepareStatement(GET_IMAGE);
			s.setLong(1, id);
			ResultSet res = s.executeQuery();
			if (res.next()) {
				if (!res.isLast()) {
					System.err.println("Result is not unique while getting image...");
				}
				String title = res.getString("title");
				List<String> tags = new ArrayList<>();
				for (String t : (String[]) res.getArray("tags").getArray()) {
					// stop-words from the database are removed
					if (!FlickrStopWordService.isAStopWord(t)) {
						tags.add(t);
					}
				}
				GeoCoords coords = new GeoCoords(res.getDouble("longitude"), res.getDouble("latitude"));
				String userId = res.getString("userid");
				Date dateTaken = res.getDate("date_taken");
				URL url;
				try {
					url = new URL(buildDownloadURL(res.getInt("flickr_farm"),
							res.getInt("flickr_server"), id, res.getString("flickr_secret")));
					return new Image(id, title, tags, coords, userId, dateTaken, url);
				} catch (MalformedURLException e) {
					throw new SQLException(); // URL should be well-formed otherwise
				}
			} else {
				return null; // image does likely not exist in the database
			}
		} else {
			throw new SQLException("Not connected to the database. Abort query.");
		}
	}
	
	private String buildDownloadURL(Integer farm, Integer server, Long id, String secret) {
		return "http://farm" + farm + ".staticflickr.com/" + server + "/"+ id + "_" + secret + ".jpg";
	}
	
}
