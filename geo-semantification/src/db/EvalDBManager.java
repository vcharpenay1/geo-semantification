package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Resource;

import model.GeoTagging;
import model.Image;
import model.ModelException;
import model.Place;
import model.SemFeatures;
import sem.EntitySelector;

public class EvalDBManager {
	
	private static final String GET_RES = "select * from eval where method = ?";
	private static final String GET_FEAT = "select * from mldata where method = ?";
	private static final String INSERT_RES = "insert into eval (im_id,swe_url,method) values (?,?,?)";
	private static final String INSERT_FEAT = "insert into mldata (im_id, p, png, n, e, ng, cng, gc, a, method) values (?,?,?,?,?,?,?,?,?,?)";
	
	protected Connection mConnection;
	
	public EvalDBManager(String dbFile) throws SQLException {
		try {
			Class.forName("org.sqlite.JDBC");
			String url = "jdbc:sqlite:" + dbFile;
			mConnection = DriverManager.getConnection(url);
		} catch (ClassNotFoundException e) {
			System.err.println("Missing SQLite JDBC driver... Abort.");
		}
	}
	
	public void insertFeatures(Image im, SemFeatures f, EntitySelector.Type sel) throws SQLException {
		if (mConnection != null) {
			PreparedStatement s = mConnection.prepareStatement(INSERT_FEAT);
			s.setLong(1, im.getId());
			s.setInt(2, f.getNbOfPlaces());
			s.setInt(3, f.getNbOfPlacesNeighborhood());
			s.setInt(4, f.getNbOfEntities());
			s.setInt(5, f.getNbOfLinkedEntities());
			s.setInt(6, f.getNbOfEntitiesNeighborhood());
			s.setInt(7, f.getNbOfCommonEntitiesNeighborhood());
			s.setDouble(8, f.getGeographicalCoherency());
			s.setDouble(9, f.getSmallestArea());
			s.setString(10, sel.toString());
			int r = s.executeUpdate();
			if (r != 1) {
				throw new SQLException("Result not inserted, " + r + " rows modified.");
			}
		} else {
			throw new SQLException("Not connected to the database. Abort inserting.");
		}
	}
	
	/**
	 * Only used to indicates which images are already processed...
	 * @param sel
	 * @return
	 * @throws SQLException 
	 */
	public Set<Long> getFeatures(EntitySelector.Type sel) throws SQLException {
		if (mConnection != null) {
			Set<Long> ids = new HashSet<>();
			PreparedStatement s = mConnection.prepareStatement(GET_FEAT);
			s.setString(1, sel.toString());
			ResultSet res = s.executeQuery();
			while (res.next()) {
				Long id = res.getLong("im_id");
				ids.add(id);
			}
			return ids;
		} else {
			throw new SQLException("Not connected to the database. Abort query.");
		}
	}
	
	public void insertResult(Image im, Place p, EntitySelector.Type sel) throws SQLException {
		if (mConnection != null) {
			String url = (p != null) ? p.getEntity().getURI() : "?";
			PreparedStatement s = mConnection.prepareStatement(INSERT_RES);
			s.setLong(1, im.getId());
			s.setString(2, url);
			s.setString(3, sel.toString());
			int r = s.executeUpdate();
			if (r != 1) {
				throw new SQLException("Result not inserted, " + r + " rows modified.");
			}
		} else {
			throw new SQLException("Not connected to the database. Abort inserting.");
		}
	}
	
	public Map<Long, GeoTagging> getResults(EntitySelector.Type sel) throws SQLException {
		if (mConnection != null) {
			Model model = ModelFactory.createDefaultModel();
			FlickrDBManager db = DBConnector.getFlickrDB();
			Map<Long, GeoTagging> results = new HashMap<>();
			PreparedStatement s = mConnection.prepareStatement(GET_RES);
			s.setString(1, sel.toString());
			ResultSet res = s.executeQuery();
			while (res.next()) {
				Long id = res.getLong("im_id");
				String url = res.getString("swe_url");
				try {
					Image im = db.getImage(id);
					Place p;
					if (!url.equals("?")) {
						p = new Place(model.createResource(url));
					} else {
						p = null;
					}
					results.put(im.getId(), new GeoTagging(im, p));
				} catch (ModelException e) {
					System.out.println(url + " is no place. Skipped.");
				}
			}
			return results;
		} else {
			throw new SQLException("Not connected to the database. Abort query.");
		}
	}

}
