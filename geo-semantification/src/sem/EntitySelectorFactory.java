package sem;

import model.Image;

public class EntitySelectorFactory {

	public static EntitySelector create(EntitySelector.Type t, Image im) {
		switch (t) {
		case baseline:
			return new VerySimpleSelector(im);
		case tfidf:
			return new SimpleContextSelector(im);
		case angeletou:
			return new AngeletouSelector(im);
		case garcia:
			return new GarciaSilvaSelector(im);
		case tesconi:
			return new TesconiSelector(im);
		case tesconigeo:
			return new TesconiGeoSelector(im);
		case meta:
			return new MetaSelector(im, EntitySelector.Type.tesconi, EntitySelector.Type.angeletou);
		case spotlight:
			return new SpotlightSelector(im);
		default:
			return null;
		}
	}
	
}
