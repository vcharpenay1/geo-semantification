package sem;

import ir.RetrievalUtil;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import model.Image;
import model.WikipediaPage;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;

import db.DBConnector;
import db.WikipediaDBManager;

/**
 * See the following article:
 * Garc�a-Silva, A., Szomszor, M., Alani, H., & Corcho, �. (2009, March 15). Preliminary Results in Tag Disambiguation using DBpedia.
 * Accessible at http://oa.upm.es/6377/1/Preliminary_Results_in_Tag_D.pdf
 *
 */
public class GarciaSilvaSelector extends EntitySelector {

	private List<String> mContext;
	private WikipediaDBManager mDb;
	
	/**
	 * 
	 * @param ctx
	 * @param db should be already connected
	 */
	public GarciaSilvaSelector(Image im) {
		super(im);
		mContext = im.getTags();
	}
	
	@Override
	public TreeMap<Double, Resource> rank(String tag) {
		try {
			WikipediaDBManager db = DBConnector.getWikipediaDB();
			TreeMap<Double, Resource> rankedPages = new TreeMap<>();
			// the selector uses the set of words {tag + its context}
			List<String> vocabulary = new ArrayList<>(mContext);
			List<WikipediaPage> pages = db.getPages(tag, true);
			if (pages.isEmpty()) {
				pages = db.getPages(tag, false);
			}
			
			double docMaxNorm = Double.MIN_VALUE; // euclidian norm of the greatest vector
			for (WikipediaPage p : pages) {
				double[] doc = new double [vocabulary.size()];
				double[] query = new double [vocabulary.size()];
				Map<String, Integer> words = p.getWords();
				double docNorm = 0;
				for (int i = 0; i < vocabulary.size(); i++) {
					query[i] = 1;
					if (words.containsKey(vocabulary.get(i))) {
						doc[i] = words.get(vocabulary.get(i));
					} else {
						doc[i] = 0.;
					}
					docNorm += doc[i] * doc[i];
				}
				Double sim = RetrievalUtil.cosSimilarity(doc, query);
				if (!rankedPages.containsKey(sim)) {
					rankedPages.put(sim, pageToEntity(p));
					if (rankedPages.lastKey() < sim) {
						docMaxNorm = docNorm;
					}
				} else if (rankedPages.lastKey() == sim && docNorm > docMaxNorm) {
					// only inserts the page that have the greatest vector norm
					rankedPages.put(sim, pageToEntity(p));
					docMaxNorm = docNorm;
				}
			}
			
			return rankedPages;
		} catch (SQLException e) {
			// most likely not connected to the DB
			return null;
		}
	}
	
	protected Resource pageToEntity(WikipediaPage p) {
		// builds an ad-hoc model containing only one entity
		Model model = ModelFactory.createDefaultModel();
		return model.createResource(wikiToDb(p.getUrl()));
	}
	
	/**
	 * Returns a DBpedia URI from a Wikipedia URL.
	 * The mapping is straightforward:
	 * "en.wikipedia.org/wiki/<i>name</i>" <-> "dbpedia.org/resource/<i>name</i>"
	 * @param url
	 * @return
	 */
	private String wikiToDb(String url) {
		String token = "wikipedia.org/wiki/";
		if (!url.contains(token)) {
			return null;
		}
		// everything after token corresponds to page's name
		String name = url.split(token)[1];
		return "http://dbpedia.org/resource/" + name;
	}

}
