package sem;

import java.util.List;
import java.util.TreeMap;

import model.Image;

import com.hp.hpl.jena.rdf.model.Resource;

/**
 * Selects the first entity in the list
 *
 */
public class VerySimpleSelector extends EntitySelector {
	
	public VerySimpleSelector(Image im) {
		super(im); // not used here
	}
	
	@Override
	public TreeMap<Double, Resource> rank(String tag) {
		TreeMap<Double, Resource> stub = new TreeMap<>();
		List<Resource> entities = SemanticsManager.getEntities(tag);
		if (!entities.isEmpty()) {
			stub.put(0., entities.get(0));
			return stub;
		} else {
			return null;
		}
	}
	
}
