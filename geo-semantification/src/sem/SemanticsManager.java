package sem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.jena.atlas.web.HttpException;

import model.GeoCoords;
import jena.rdfcat;

import com.hp.hpl.jena.datatypes.BaseDatatype;
import com.hp.hpl.jena.datatypes.RDFDatatype;
import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.graph.NodeFactory;
import com.hp.hpl.jena.graph.Triple;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.sparql.expr.E_Str;
import com.hp.hpl.jena.sparql.expr.E_StrStartsWith;
import com.hp.hpl.jena.sparql.expr.Expr;
import com.hp.hpl.jena.sparql.expr.ExprVar;
import com.hp.hpl.jena.sparql.expr.nodevalue.NodeValueString;
import com.hp.hpl.jena.sparql.pfunction.library.concat;
import com.hp.hpl.jena.sparql.syntax.Element;
import com.hp.hpl.jena.sparql.syntax.ElementFilter;
import com.hp.hpl.jena.sparql.syntax.ElementGroup;
import com.hp.hpl.jena.sparql.syntax.ElementTriplesBlock;
import com.hp.hpl.jena.sparql.syntax.ElementUnion;
import com.hp.hpl.jena.vocabulary.OWL;
import com.hp.hpl.jena.vocabulary.RDF;
import com.hp.hpl.jena.vocabulary.RDFS;

public class SemanticsManager {

	private static String sEndPoint;
	
	public static void init(String endpoint) {
		sEndPoint = endpoint;
	}
	
	/**
	 * Get all the entities from the end point that have the given tag
	 * as a label. Restricted to DBpedia entities.
	 * <code>SELECT ?e WHERE { ?e rdfs:label % . 
	 * FILTER ( STRSTARTS( STR(?e), "http://dbpedia.org" ) ) }</code>
	 * @param tag
	 * @return
	 */
	public static List<Resource> getEntities(String tag) {
		List<Resource> entities = new ArrayList<>();
		
		// builds the query
		ElementGroup body = new ElementGroup();
		Expr expr = new E_StrStartsWith(new E_Str(new ExprVar("e")), 
				new NodeValueString("http://dbpedia.org")); // DBPedia filtering
		ElementFilter filter = new ElementFilter(expr);
		body.addElementFilter(filter);
		ElementUnion union = new ElementUnion();
		// several variations of the word are generated
		for (String t : getVariation(tag)) {
			ElementTriplesBlock triple = new ElementTriplesBlock();
			Node subj = NodeFactory.createVariable("e");
			Node pred = NodeFactory.createURI(RDFS.label.getURI());
			Node obj = NodeFactory.createLiteral(t); // query parameterization
			triple.addTriple(new Triple(subj, pred, obj));
			union.addElement(triple);
		}
		body.addElement(union);
		Query q = new Query();
		q.setQueryPattern(body);
		q.setQuerySelectType();
		q.addResultVar("e");
		
		// executes the query and processes the result
		ResultSet set = tryExecute(q);
		while (set != null && set.hasNext()) {
			QuerySolution sol = set.nextSolution();
			RDFNode node = sol.get("e");
			if (node.isResource()) {
				entities.add((Resource) node);
			} else {
				// not deterministic behavior. Same request, sometimes
				// nodes that are no Resource in the result...
				System.err.println(node + " is no Resource. Skipped.");
			}
		}
		
		return entities;
	}

	/**
	 * Retrieves the neighbors from the given entity. The neighbors are the nodes
	 * that are linked to the given entity by a triple (either as object or
	 * subject).
	 * @param e
	 * @return
	 */
	public static List<RDFNode> getNeighbors(Resource ent) {
		if (!isRequestable(ent)) {
			return null;
		}
		List<RDFNode> ngb = new ArrayList<>();
		
		Resource redirect = checkRedirect(ent);
		if (redirect != null) {
			System.out.println(ent.getURI() + " redirects to " + redirect.getURI() + "...");
			return getNeighbors(redirect);
		}

		Node eVar = NodeFactory.createVariable("e");
		Node pVar = NodeFactory.createVariable("p");
		Node entNode = NodeFactory.createURI(ent.getURI()); // query parameterization
		ElementTriplesBlock block1 = new ElementTriplesBlock();
		block1.addTriple(new Triple(eVar, pVar, entNode));
		ElementTriplesBlock block2 = new ElementTriplesBlock();
		block2.addTriple(new Triple(entNode, pVar, eVar));
		ElementUnion body = new ElementUnion();
		body.addElement(block1);
		body.addElement(block2);
		Query q = new Query();
		q.setQueryPattern(body);
		q.setQuerySelectType();
		q.addResultVar("e");
		q.addResultVar("p");
		
		ResultSet set = tryExecute(q);
		while (set != null && set.hasNext()) {
			ngb.add(set.nextSolution().get("e"));
		}
		
		return ngb;
	}
	
	/**
	 * Similarly to <code>getNeighbors()</code>, retrieves neighbors
	 * of the given entity. The targetted neighbors are:
	 * - superclasses, subclasses for classes (or type of the instance)
	 * - range, domain, superproperties, subproperties for properties
	 * - ? for instances
	 * @param ent
	 * @return
	 */
	public static List<Resource> getSemanticNeighbors(Resource ent) {
		if (!isRequestable(ent)) {
			return null;
		}
		List<Resource> ngb = new ArrayList<>();
		
		ElementUnion body = new ElementUnion();
		Node eVar = NodeFactory.createVariable("e");
		Node entNode = NodeFactory.createURI(ent.getURI());
		List<Resource> entTypes = getEntityTypes(ent);
		if (entTypes.contains(OWL.Class) || entTypes.contains(RDFS.Class)) { // ent is a class
			// pattern for subclasses
			Node subNode = NodeFactory.createURI(RDFS.subClassOf.getURI());
			ElementTriplesBlock block1 = new ElementTriplesBlock();
			block1.addTriple(new Triple(eVar, subNode, entNode));
			body.addElement(block1);
			
			// pattern for superclasses
			ElementTriplesBlock block2 = new ElementTriplesBlock();
			block2.addTriple(new Triple(entNode, subNode, eVar));
			body.addElement(block2);
			
			// pattern for disjoint classes
			Node disNode = NodeFactory.createURI(OWL.disjointWith.getURI());
			ElementTriplesBlock block3 = new ElementTriplesBlock();
			block3.addTriple(new Triple(entNode, disNode, eVar));
			body.addElement(block3);
		} else if (entTypes.contains(RDF.Property)) { // ent is a property
			// pattern for subproperties
			Node subNode = NodeFactory.createURI(RDFS.subPropertyOf.getURI());
			ElementTriplesBlock block1 = new ElementTriplesBlock();
			block1.addTriple(new Triple(eVar, subNode, entNode));
			body.addElement(block1);

			// pattern for superproperties
			ElementTriplesBlock block2 = new ElementTriplesBlock();
			block2.addTriple(new Triple(entNode, subNode, eVar));
			body.addElement(block2);
			
			// pattern for domain
			Node domNode = NodeFactory.createURI(RDFS.domain.getURI());
			ElementTriplesBlock block3 = new ElementTriplesBlock();
			block3.addTriple(new Triple(entNode, domNode, eVar));
			body.addElement(block3);
			
			// pattern for range
			Node rangeNode = NodeFactory.createURI(RDFS.range.getURI());
			ElementTriplesBlock block4 = new ElementTriplesBlock();
			block4.addTriple(new Triple(entNode, rangeNode, eVar));
			body.addElement(block4);
		} else { // ent is an instance ?
			// special case: the semantic neighborhood is defined
			// as the types associated to ent
			// TODO extend to the semantic neighborhood of the resource's class ?
			return entTypes;
		}
		Query q = new Query();
		q.setQueryPattern(body);
		q.setQuerySelectType();
		q.addResultVar("e");
		
		ResultSet set = tryExecute(q);
		while (set != null && set.hasNext()) {
			ngb.add(set.nextSolution().getResource("e"));
		}
		return ngb;
	}
	
	/**
	 * Get the parent(s) of the given entity. Parents are:
	 * - superclasses for classes
	 * - superproperties for properties
	 * - classes for instances
	 * @param ent
	 * @return
	 */
	public static List<Resource> getOntologicalParents(Resource ent) {
		if (!isRequestable(ent)) {
			return null;
		}
		List<Resource> parents = new ArrayList<>();
		List<Resource> ngb = new ArrayList<>();
		
		ElementTriplesBlock body = new ElementTriplesBlock();
		Node eVar = NodeFactory.createVariable("e");
		Node entNode = NodeFactory.createURI(ent.getURI());
		List<Resource> entTypes = getEntityTypes(ent);
		if (entTypes.contains(OWL.Class) || entTypes.contains(RDFS.Class) || entTypes.isEmpty()) {
			// ent is (most likely) a class
			// TODO also check if ent is a property, if entTypes is empty
			Node subNode = NodeFactory.createURI(RDFS.subClassOf.getURI());
			body.addTriple(new Triple(entNode, subNode, eVar));
		} else if (entTypes.contains(RDF.Property)) { // ent is a property
			Node subNode = NodeFactory.createURI(RDFS.subPropertyOf.getURI());
			body.addTriple(new Triple(entNode, subNode, eVar));
		} else { // ent is an instance ?
			return entTypes;
		}
		Query q = new Query();
		q.setQueryPattern(body);
		q.setQuerySelectType();
		q.addResultVar("e");
		
		ResultSet set = tryExecute(q);
		while (set != null && set.hasNext()) {
			parents.add(set.nextSolution().getResource("e"));
		}
		
		return parents;
	}
	
	/**
	 * Retrieves the type(s) of the given entity.
	 * @param ent
	 * @return
	 */
	public static List<Resource> getEntityTypes(Resource ent) {
		if (!isRequestable(ent)) {
			return null;
		}
		List<Resource> types = new ArrayList<>();
		
		Resource redirect = checkRedirect(ent);
		if (redirect != null) {
			System.out.println(ent.getURI() + " redirects to " + redirect.getURI() + "...");
			return getEntityTypes(redirect);
		}
		
		ElementTriplesBlock block = new ElementTriplesBlock();
		Node subj = NodeFactory.createURI(ent.getURI()); // query parameterization
		Node pred = NodeFactory.createURI(RDF.type.getURI());
		Node obj = NodeFactory.createVariable("t");
		block.addTriple(new Triple(subj, pred, obj));
		Query q = new Query();
		q.setQueryPattern(block);
		q.setQuerySelectType();
		q.addResultVar("t");
		
		ResultSet set = tryExecute(q);
		while (set != null && set.hasNext()) {
			QuerySolution sol = set.nextSolution();
			RDFNode node = sol.get("t");
			if (node.isResource()) {
				types.add((Resource) node);
			} else {
				// not deterministic behavior. Same request, sometimes
				// nodes that are no Resource in the result...
				System.err.println(node + " is no Resource. Skipped.");
			}
		}
		
		return types;
	}
	
	/**
	 * Returns the coordinates of the given resource, if the latter
	 * is of type SpatialThing (W3C specification). It returns
	 * null otherwise
	 * Corresponding predicates are geo:lat and geo:long
	 * TODO try other predicates (dbprop:latitude, dbprop:longitude)
	 * @param ent
	 * @return
	 */
	public static GeoCoords getLocation(Resource ent) {
		if (!isRequestable(ent)) {
			return null;
		}
		String wgs84 = "http://www.w3.org/2003/01/geo/wgs84_pos#";
		Float lat = null, lon = null;
		Node subj = NodeFactory.createURI(ent.getURI()); // query parameterization
		
		ElementTriplesBlock block = new ElementTriplesBlock();
		Node pred = NodeFactory.createURI(wgs84 + "lat");
		Node obj = NodeFactory.createVariable("lat");
		block.addTriple(new Triple(subj, pred, obj));
		Query q = new Query();
		q.setQueryPattern(block);
		q.setQuerySelectType();
		q.addResultVar("lat");

		ResultSet set = tryExecute(q);
		if (set == null) {
			return null;
		}
		if (set.hasNext()) {
			QuerySolution sol = set.nextSolution();
			Literal node = (Literal) sol.get("lat");
			lat = node.getFloat();
		}
		
		block = new ElementTriplesBlock();
		pred = NodeFactory.createURI(wgs84 + "long");
		obj = NodeFactory.createVariable("lon");
		block.addTriple(new Triple(subj, pred, obj));
		q = new Query();
		q.setQueryPattern(block);
		q.setQuerySelectType();
		q.addResultVar("lon");

		set = tryExecute(q);
		if (set == null) {
			return null;
		}
		if (set.hasNext()) {
			QuerySolution sol = set.nextSolution();
			Literal node = (Literal) sol.get("lon");
			lon = node.getFloat();
		}
		
		if (lat != null && lon != null) {
			return new GeoCoords(lon, lat);
		} else {
			Resource redirect = checkRedirect(ent);
			if (redirect != null) {
				return getLocation(redirect);
			}
		}
		return null;
	}
	
	/**
	 * Returns the area of the entity, if it is
	 * a (subtype of) Place or null if not.
	 * Possible corresponding predicates are
	 *   dbpedia-owl:area
	 *   dbpedia-owl:PopulatedPlace/areaTotal
	 *   dbpprop:areaSqMi
	 *   dbpprop:areaKm
	 *   dbpedia-owl:areaTotal
	 * @param ent
	 * @return
	 */
	public static Double getArea(Resource ent) {
		if (!isRequestable(ent)) {
			return null;
		}
		Map<String, Double> preds = new HashMap<>();
		// conversion factors to km2
		preds.put("http://dbpedia.org/ontology/area", 0.000001); // m2
		preds.put("http://dbpedia.org/property/area", 1.); // km2
		preds.put("http://dbpedia.org/ontology/PopulatedPlace/areaTotal", 1.); // km2
		preds.put("http://dbpedia.org/property/areaKm", 1.); // km2
		preds.put("http://dbpedia.org/property/areaSqMi", 2.589988110336); // mile2

		Node subj = NodeFactory.createURI(ent.getURI()); // query parameterization
		Node obj = NodeFactory.createVariable("a");
		for (String p : preds.keySet()) {
			ElementTriplesBlock block = new ElementTriplesBlock();
			Node pred = NodeFactory.createURI(p);
			block.addTriple(new Triple(subj, pred, obj));
			Query q = new Query();
			q.setQueryPattern(block);
			q.setQuerySelectType();
			q.addResultVar("a");
			
			ResultSet set = tryExecute(q);
			if (set == null) {
				return null;
			}
			if (set.hasNext()) {
				try {
					QuerySolution sol = set.nextSolution();
					Object val = sol.getLiteral("a").getValue();
					Double area = null;
					if (val.getClass().equals(BaseDatatype.TypedValue.class)) {
						area = Double.parseDouble(((BaseDatatype.TypedValue) val).lexicalValue); 
					} else {
						// expected of class Number (Integer, Float, Double...)
						area = Double.parseDouble(val.toString());
					}
					area *= preds.get(p); // conversion to km2
					return area;
				} catch (NumberFormatException | ClassCastException e) {
					System.err.println("Could not parse value to double. Skipped.");
				}
			}
		}

		Resource redirect = checkRedirect(ent);
		if (redirect != null) {
			return getArea(redirect);
		}
		return null;
	}
	
	/**
	 * Get the label(s) from the entity (if found in the RDF graph)
	 * @param e
	 * @return
	 */
	public static List<String> findLabels(Resource ent) {
		if (!isRequestable(ent)) {
			return null;
		}
		List<String> labels = new ArrayList<>();

		ElementTriplesBlock block = new ElementTriplesBlock();
		Node subj = NodeFactory.createURI(ent.getURI()); // query parameterization
		Node pred = NodeFactory.createURI(RDFS.label.getURI());
		Node obj = NodeFactory.createVariable("l");
		block.addTriple(new Triple(subj, pred, obj));
		Query q = new Query();
		q.setQueryPattern(block);
		q.setQuerySelectType();
		q.addResultVar("l");
		
		ResultSet set = tryExecute(q);
		while (set != null && set.hasNext()) {
			QuerySolution sol = set.nextSolution();
			labels.add(sol.getLiteral("l").getString());
		}
		
		return labels;
	}
	
	/**
	 * Filters out all the entities which URL is not found or does not
	 * have any RDF attribute.
	 * @param entities
	 * @return
	 */
	public static Boolean valid(Resource ent) {
		if (!isRequestable(ent)) {
			return null;
		}
		ElementTriplesBlock block = new ElementTriplesBlock();
		Node subj = NodeFactory.createURI(ent.getURI()); // query parameterization
		Node pred = NodeFactory.createVariable("a");
		Node obj = NodeFactory.createVariable("x");
		block.addTriple(new Triple(subj, pred, obj));
		Query q = new Query();
		q.setQueryPattern(block);
		q.setQuerySelectType();
		q.addResultVar("a");
		q.addResultVar("x");
		
		ResultSet set = tryExecute(q);
		return (set != null && set.hasNext()); // true if any result has been found
	}
	
	/**
	 * Returns a list of probable variations of the label that can be
	 * found in an entity's property
	 * @param label
	 * @return
	 */
	private static List<String> getVariation(String label) {
		List<String> labels = new ArrayList<>();
		Character firstChar = label.charAt(0);
		String remaining = label.substring(1);
		labels.add(Character.toLowerCase(firstChar) + remaining);
		labels.add(Character.toUpperCase(firstChar) + remaining);
		return labels;
	}
	
	/**
	 * Check if the given resource is only a stub and contains
	 * a dbpedia-owl:wikiPageRedirects. If so, returns the redirected
	 * resource, null otherwise
	 * @param ent
	 * @return
	 */
	private static Resource checkRedirect(Resource ent) {
		if (!isRequestable(ent)) {
			return null;
		}
		ElementTriplesBlock block = new ElementTriplesBlock();
		Node subj = NodeFactory.createURI(ent.getURI());
		Node pred = NodeFactory.createURI("http://dbpedia.org/ontology/wikiPageRedirects");
		Node obj = NodeFactory.createVariable("r");
		block.addTriple(new Triple(subj, pred, obj));
		Query q = new Query();
		q.setQueryPattern(block);
		q.setQuerySelectType();
		q.addResultVar("r");

		ResultSet set = tryExecute(q);
		if (set != null && set.hasNext()) {
			QuerySolution sol = set.nextSolution();
			Resource redirect = sol.getResource("r");
			return redirect;
		} else {
			return null;
		}
	}
	
	private static ResultSet tryExecute(Query q) {
		try {
			return QueryExecutionFactory.sparqlService(sEndPoint, q).execSelect();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * Checks not-allowed chars,
	 * otherwise leads to HTTP 400 in execSelect()
	 * @param ent
	 * @return
	 */
	private static Boolean isRequestable(Resource ent) {
		return !(ent.getURI().contains("\\")
				|| ent.getURI().contains("\""));
	}
	
}
