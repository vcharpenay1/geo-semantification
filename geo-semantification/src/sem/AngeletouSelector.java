package sem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import model.Image;

import com.hp.hpl.jena.rdf.model.Resource;

import edu.cmu.lti.jawjaw.db.SenseDAO;
import edu.cmu.lti.jawjaw.db.SynlinkDAO;
import edu.cmu.lti.jawjaw.db.SynsetDAO;
import edu.cmu.lti.jawjaw.db.WordDAO;
import edu.cmu.lti.jawjaw.pobj.Lang;
import edu.cmu.lti.jawjaw.pobj.Link;
import edu.cmu.lti.jawjaw.pobj.POS;
import edu.cmu.lti.jawjaw.pobj.Sense;
import edu.cmu.lti.jawjaw.pobj.Synlink;
import edu.cmu.lti.jawjaw.pobj.Synset;
import edu.cmu.lti.jawjaw.pobj.Word;
import edu.cmu.lti.jawjaw.util.WordNetUtil;
import edu.cmu.lti.lexical_db.NictWordNet;
import edu.cmu.lti.lexical_db.data.Concept;
import edu.cmu.lti.ws4j.impl.WuPalmer;

/**
 * See the following article:
 * Angeletou, S., Sabou, M., & Motta, E. (2008, November 17). Semantically enriching folksonomies with FLOR.
 * Accessible at http://oro.open.ac.uk/23497/1/10.1.1.142.7787[1].pdf
 *
 */
public class AngeletouSelector extends EntitySelector {

	/**
	 * weight of the lexical similarity in the clustering step
	 */
	private static final Double W_L = 0.3;
	/**
	 * weight of the graph similarity in the clustering step
	 */
	private static final Double W_G = 0.7;
	/**
	 * a threshold used to build entities clusters
	 */
	private static final Double SIM_THRESHOLD = 0.5;
	
	private List<String> mContext;
	private Map<String, List<String>> mCleanedContext;
	
	/**
	 * this classes are from a stand-alone library that
	 * uses WordNet 3.0 for the specific calculation
	 * of Wu & Palmer similarity
	 */
	private NictWordNet mWN;
	private WuPalmer mSimCalculator;
	private Synset mDisambiguated;
	
	public AngeletouSelector(Image im) {
		super(im); // im is not used in this selector...
		mWN = new NictWordNet();
		mCleanedContext = new HashMap<>();
		for (String t : im.getTags()) {
			mCleanedContext.put(t, clean(t));
		}
		mSimCalculator = new WuPalmer(mWN);
	}
	
	/**
	 * For analysis/debugging purpose
	 * @return
	 */
	public Map<String, List<String>> getCleanedContext() {
		return mCleanedContext;
	}
	
	@Override
	public TreeMap<Double, Resource> rank(String tag) {
		// initialize the extended context
		if (!mCleanedContext.get(tag).isEmpty()) {
			mContext = new ArrayList<>();
			for (Entry<String, List<String>> e : mCleanedContext.entrySet()) {
				if (!e.getKey().equals(tag) || e.getValue().size() > 1) {
					for (String s : e.getValue()) {
						mContext.add(s);
					}
				}
			}
		} else {
			return null; // tag not found in WordNet
		}
		
		// entities retrieval
		List<Resource> relevantEntities = new ArrayList<>();
		mDisambiguated = disambiguate(tag);
		if (mDisambiguated == null) {
			return null; // tag not found in WordNet
		} else {
			for (String syn : getSynonyms(mDisambiguated)) {
				relevantEntities.addAll(SemanticsManager.getEntities(syn));
			}
		
			// entities clustering
			List<Set<Resource>> clusters = cluster(relevantEntities);
		
			// Hypernyms /vs/ ontological parents selection
			return compareSem(clusters);
		}
	}
	
	/**
	 * Returns the sense of the given tag according to current context,
	 * thanks to WordNet.
	 * @param tag
	 * @return
	 */
	private Synset disambiguate(String tag) {
		List<Synset> senses = browsePOS(tag);
		if (senses.size() == 0) {
			System.err.println("The tag '" + tag + "' was not found in WordNet...");
			return null;
		}
		// note: List<Synset> is used to store a pair of senses
		// the tag's senses are at index 0, context's sense at index 1
		TreeMap<Double, List<Synset>> sims = new TreeMap<>();
		for (String w : mContext) {
			List<Synset> ctxSenses = browsePOS(w);
			if (ctxSenses.size() > 0) {
				for (Synset ctxs : ctxSenses) {
					for (Synset s : senses) {
						List<Synset> pair = new ArrayList<>();
						pair.add(s);
						pair.add(ctxs);
						Double wup = mSimCalculator.calcRelatednessOfSynset(new Concept(s.getSynset()), new Concept(ctxs.getSynset())).getScore();
						if (!sims.containsKey(wup)) {
							// if pairs have an equal similarity, only the first one is considered
							sims.put(wup, pair);
						}
					}
				}
			} else {
				System.err.println("The tag '" + w + "' was not found in WordNet...");
			}
		}
		
		if (sims.isEmpty()) { // no single tag in the context found in WordNet
			return null;
		} else {
			return sims.lastEntry().getValue().get(0);
		}
	}

	/**
	 * Performs 3 tries to find the given word in WordNet
	 * @param word
	 * @return
	 */
	private List<Synset> browsePOS(String word) {
		List<Synset> senses = new ArrayList<>();
		List<POS> pos = new ArrayList<>();
		pos.add(POS.n);
		pos.add(POS.a);
		pos.add(POS.r);
		pos.add(POS.v);
		for (POS p : pos) {
			for (Synset s : WordNetUtil.wordToSynsets(word, p)) {
				s.setPos(p);
				senses.add(s);
			}
		}
		
		return senses;
	}
	
	private List<String> getSynonyms(Synset synset) {
		List<String> synonyms = new ArrayList<>();
		for (Sense s : SenseDAO.findSensesBySynset(synset.getSynset())) {
			Word syn = WordDAO.findWordByWordid(s.getWordid());
			//note: WS4J, the library we use here, also contains Japanese WordNet
			if (syn.getLang().equals(Lang.eng) && syn.getLemma().contains("_")) {
				for (String part : syn.getLemma().split("_")) {
					synonyms.add(part);
				}
			} else if (syn.getLang().equals(Lang.eng)) {
				synonyms.add(syn.getLemma());
			}
		}
		
		return synonyms;
	}
	
	/**
	 * Builds sets of entities that are considered similar according
	 * to a mixed similarity measure based on lexical and semantic
	 * features
	 * @param list
	 * @return
	 */
	private List<Set<Resource>> cluster(List<Resource> list) {
		List<Set<Resource>> clusters = new ArrayList<>();
		// initialization: all resources are put in a set containing a single element
		for (Resource r : list) {
			Set<Resource> singleton = new HashSet<>();
			singleton.add(r);
			clusters.add(singleton);
		}
		
		EditDistance editDist = new EditDistance();
		Integer prevClusterNb = Integer.MAX_VALUE;

//		// iterates until no more simplification/clustering is performed
//		while (clusters.size() < prevClusterNb) {
//			prevClusterNb = clusters.size();
//			// stores which set should be removed after merging
//			// this is done out of the double-loop
//			List<Integer> toRemove = new ArrayList<>();
//			
//			for (int i = 0; i < clusters.size(); i++) {
//				for (int j = i + 1; j < clusters.size(); j++) {
//					Set<Resource> set1 = clusters.get(i);
//					Set<Resource> set2 = clusters.get(j);
//					
//					// computes lexical similarity for each pair
//					Double lexicalSim = 0.;
//					for (Resource r1 : set1) {
//						for (Resource r2 : set2) {
//							String name1 = r1.getLocalName().toLowerCase();
//							String name2 = r2.getLocalName().toLowerCase();
//							Double maxLength = (double) Math.max(name1.length(), name2.length());
//							// defined this way, the similarity value falls within [0,1]
//							lexicalSim += (maxLength - editDist.compute(name1, name2)) / maxLength;
//						}
//					}
//					lexicalSim /= set1.size() * set2.size();
//					
//					// computes graph similarity by merging the neighborhood
//					// of all entities in a cluster
//					List<Resource> ngbs1 = new ArrayList<>();
//					for (Resource r1 : set1) {
//						ngbs1.addAll(SemanticsManager.getSemanticNeighbors(r1));
//					}
//					List<Resource> ngbs2 = new ArrayList<>();
//					for (Resource r2 : set2) {
//						ngbs2.addAll(SemanticsManager.getSemanticNeighbors(r2));
//					}
//					Double graphSim = 0.;
//					for (Resource ngb1 : ngbs1) {
//						for (Resource ngb2 : ngbs2) {
//							String ngbName1 = ngb1.getLocalName().toLowerCase();
//							String ngbName2 = ngb2.getLocalName().toLowerCase();
//							// string similarity <-> one word contains the other ?
//							if (ngbName1.contains(ngbName2) || ngbName2.contains(ngbName1)) {
//								graphSim++;
//							}
//						}
//					}
//					if (ngbs1.size() > 0 && ngbs2.size() > 0) {
//						// some neighborhoods are empty in the triple store
//						graphSim /= ngbs1.size() * ngbs2.size();
//					}
//					
//					// computes global similarity & fuse the entities if needed
//					Double sim = W_L * lexicalSim + W_G * graphSim;
//					if (sim > SIM_THRESHOLD) {
//						System.out.println(set1 + " and " + set2 + " have been fused,"
//								+ " with a similarity of " + sim);
//						// merges set1 and set2
//						set1.addAll(set2);
//						// indicates that set2 should be removed when iteration
//						// is over by giving set2's index in the collection
//						toRemove.add(j);
//					}
//				}
//			}
//			
//			for (Integer i : toRemove) {
//				clusters.remove(i);
//			}
//		}
		
		return clusters;
	}
	
	/**
	 * Selects the most relevant entity (or group of similar entities)
	 * by comparing their ontological parents and their hypernyms as
	 * defined in WordNet.
	 * @param clusters
	 * @return
	 */
	private TreeMap<Double, Resource> compareSem(List<Set<Resource>> clusters) {
		if (clusters.isEmpty()) {
			return null;
		}
		
		TreeMap<Double, Resource> counts = new TreeMap<>();

		Synset hype = getHypernym(mDisambiguated);
		List<String> hypes;
		if (hype == null) {
			// the disambiguated synset is already a root (ex: proper nouns)
			// the selection is performed directly on the synset instead of
			// its hypernym
			hypes = getSynonyms(mDisambiguated);
		} else {
			hypes = getSynonyms(hype);
		}
		for (Set<Resource> set : clusters) {
			Integer card = 0;
			TreeMap<Integer, Resource> setCounts = new TreeMap<>();
			for (Resource r : set) {
				Integer rCount = 0;
				List<Resource> parents;
//				if (hype == null) {
					// the same as upper.
					// the selection is performed on the entity itself
					// instead of its ontological parents
//					parents = new ArrayList<>();
//					parents.add(r);
//				} else {
					parents = SemanticsManager.getOntologicalParents(r);
//				}
				// computes the cardinality of the intersection of hypes and parents
				for (Resource parent : parents) {
					for (String h : hypes) {
						h = h.toLowerCase();
						String p = parent.getLocalName().toLowerCase();
						if (h.contains(p) || p.contains(h)) {
							card++;
							rCount++;
						}
					}
				}
				setCounts.put(rCount, r); // duplicates are erased
			}
			// the most representative entity is the one that have the
			// largest intersection with hype
			counts.put((double) card, setCounts.lastEntry().getValue()); // duplicates are erased
		}
		
		return counts;
	}
	
	private Synset getHypernym(Synset synset) {
		List<Synlink> links = SynlinkDAO.findSynlinksBySynset(synset.getSynset());
		for (Synlink l : links) {
			if (l.getLink().equals(Link.hype)) {
				return SynsetDAO.findSynsetBySynset(l.getSynset2());
			}
		}
		return null;
	}
	
	/**
	 * Filters and process tag with WordNet. If a tag,
	 * its lemma or its sub-parts cannot be found in WordNet,
	 * removes it from the list.
	 * @param tags
	 * @return
	 */
	private List<String> clean(String tag) {
		List<String> cleaned = new ArrayList<>();
		
		if (!browsePOS(tag).isEmpty()) {
			cleaned.add(tag);
		} else {
			String lemma = lemmatize(tag);
			if (!lemma.equals(tag) && !browsePOS(lemma).isEmpty()) {
				cleaned.add(lemma);
			} else {
				List<String> s = split(tag);
				if (s != null) {
					cleaned = s;
				}
				// else, cleaned is returned empty
			}
		}
		
		return cleaned;
	}
	
	private String lemmatize(String word) {
		Stemmer stemmer = new Stemmer();
		stemmer.add(word.toCharArray(), word.length());
		stemmer.stem();
		String lemma = stemmer.toString();
		return lemma;
	}
	
	private List<String> split(String word) {
		if (word.equals("") || word.length() <= 2) {
			return new ArrayList<>();
		} if (!browsePOS(word).isEmpty()) {
			List<String> s = new ArrayList<>();
			s.add(word);
			return s;
		} else {
			String first, second;
			// discard all one- and two-letters words
			// (mostly alphabet letters and chemical elements)
			// misses prepositions like 'of', 'by', 'a'...
			Integer splitIndex = 3;
			
			do {
				first = word.substring(0, splitIndex);
				second = word.substring(splitIndex);
				if (!browsePOS(first).isEmpty()) {
					List<String> splits = split(second);
					if (splits != null) {
						splits.add(first);
						return splits;
					}
				}
				splitIndex++;
			} while (splitIndex < word.length());
		}
		
		return null;
	}

}
