package sem;

import java.io.BufferedReader;
import java.io.IOError;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;

public class VirtuosoHttpService {

	private String mEndPoint;
	
	public VirtuosoHttpService(String endPoint) {
		mEndPoint = endPoint;
	}
	
	public JsonObject executeQuery(String q) {
		JsonObject result = new JsonObject();
		
		URL url;
		try {
			url = new URL(mEndPoint + "?"
						  + encode("query", q) + "&"
						  + encode("format", "application/sparql-results+json"));
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			JsonReader reader = new JsonReader(in);
			result = (JsonObject) new JsonParser().parse(reader);
		} catch (MalformedURLException e) {
			System.err.println("The given endpoint address is invalid.");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("IO exception while querying the SparQL server.");
			e.printStackTrace();
		}
		
		return result;
	}
	
	private String encode(String name, String value) {
		String param = "";
		try {
			param += URLEncoder.encode(name, "UTF-8");
			param += "=";
			param += URLEncoder.encode(value, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return param;
	}

}
