package sem;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeMap;

import model.Image;

import com.hp.hpl.jena.rdf.model.Resource;

public class MetaSelector extends EntitySelector {
	
	private EntitySelector sel1;
	private EntitySelector sel2;
	
	public MetaSelector(Image im, EntitySelector.Type t1, EntitySelector.Type t2) {
		super(im);
		sel1 = EntitySelectorFactory.create(t1, im);
		sel2 = EntitySelectorFactory.create(t2, im);
	}
	
	/**
	 * Check the two rankings and selects the one that have the highest
	 * (first-second)/(second-last) ratio, where first, second and last
	 * are keys in the ranking.
	 */
	@Override
	public TreeMap<Double, Resource> rank(String tag) {
		TreeMap<Double, Resource> rank1 = sel1.rank(tag);
		TreeMap<Double, Resource> rank2 = sel2.rank(tag);
		
		if (rank1 == null && rank2 != null) {
			return rank2;
		} else if (rank1 != null && rank2 == null) {
			return rank1;
		} else if (rank1 == null && rank2 == null) {
			return null;
		}
		
		Double first1, second1, ratio1 = 0.;
		Iterator<Double> it1 = rank1.descendingKeySet().iterator();
		if (it1.hasNext()) {
			first1 = it1.next();
			if (it1.hasNext()) {
				second1 = it1.next();
				if (it1.hasNext()) {
					ratio1 = (first1 - second1) / (second1 - rank1.firstKey());
				} else {
					ratio1 = 1.;
				}
			}
		}

		Double first2, second2, ratio2 = 0.;
		Iterator<Double> it2 = rank2.descendingKeySet().iterator();
		if (it2.hasNext()) {
			first2 = it2.next();
			if (it2.hasNext()) {
				second2 = it2.next();
				if (it2.hasNext()) {
					ratio2 = (first2 - second2) / (second2 - rank2.firstKey());
				} else {
					ratio2 = 1.;
				}
			}
		}
		
		return (ratio1 >= ratio2) ? rank1 : rank2;
	}

}
