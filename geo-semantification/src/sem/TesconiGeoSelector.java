package sem;

import ir.RetrievalUtil;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.apache.http.HttpException;

import rpc.FlickrApiManager;
import model.Image;
import model.WikipediaPage;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;

import db.WikipediaDBManager;

/**
 * Do the same as {@link TesconiSelector}, except that instead of selecting
 * the first entity in the final ranking, tries to find the best geographical
 * entity (from Place ontology).
 *
 */
public class TesconiGeoSelector extends TesconiSelector {

	public TesconiGeoSelector(Image im) {
		super(im);
	}

	/**
	 * Adds a geographical criteria to Tesconi's algorithm by trying to
	 * find entities that belong to the Place ontology. If some exist,
	 * selects them.
	 * TODO improve ?
	 * @param rankedPages
	 * @return
	 */
	public Resource select(String tag) {
		TreeMap<Double, Resource> rankedPages = rank(tag);
		Model model = ModelFactory.createDefaultModel();
		Resource place = model.createResource("http://dbpedia.org/ontology/Place");
		// from the highest to the lowest sense rank
		for (Double sr : rankedPages.descendingKeySet()) {
			Resource res = rankedPages.get(sr);
			if (SemanticsManager.getEntityTypes(res).contains(place)) {
				return res;
			}
		}
		return rankedPages.lastEntry().getValue();
	}

}
