package sem;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import model.Image;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Resource;

public class SpotlightSelector extends EntitySelector {

	private static final String WS_URL = "http://spotlight.dbpedia.org/rest/annotate";

	public SpotlightSelector(Image im) {
		super(im);
	}
	
	@Override
	public TreeMap<Double, Resource> rank(String tag) {
		TreeMap<Double, Resource> stub = new TreeMap<>();
		stub.put(1., query(tag));
		return stub;
	}
	
	private Resource query(String tag) {
		try {
			String text = "";
			for (int i = 0; i < mIm.getTags().size(); i++) {
				text += mIm.getTags().get(i);
				if (i < mIm.getTags().size() - 1) {
					text += " ";
				}
			}
			text = URLEncoder.encode(text, "UTF-8");
			Document doc = Jsoup.connect(WS_URL + "?" + "text=" + text).get();
			
			Map<String, Resource> entities = new HashMap<>();
			Model model = ModelFactory.createDefaultModel();
			for (Element e : doc.getElementsByTag("a")) {
				entities.put(e.text(), model.createResource(e.attr("href")));
			}
			if (entities.containsKey(tag)) {
				System.out.println(entities.get(tag).getURI() + " was selected for " + tag);
				return entities.get(tag);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
