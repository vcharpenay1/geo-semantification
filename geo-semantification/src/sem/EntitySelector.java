package sem;

import java.util.Map;
import java.util.TreeMap;

import model.Image;

import com.hp.hpl.jena.rdf.model.Resource;

public abstract class EntitySelector {
	
	public enum Type {baseline, tfidf, angeletou, garcia, tesconi, tesconigeo, meta, spotlight};
	
	protected Image mIm;
	
	public EntitySelector(Image im) {
		mIm = im;
	}
	
	public Resource select(String tag) {
		TreeMap<Double, Resource> ranks = rank(tag);
		if (ranks != null && !ranks.isEmpty()) {
			return ranks.lastEntry().getValue();
		} else {
			return null;
		}
	}
	
	public abstract TreeMap<Double, Resource> rank(String tag);
	
}
