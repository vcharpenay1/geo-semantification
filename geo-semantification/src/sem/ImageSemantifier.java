package sem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import model.Image;
import model.SemFeatures;

import com.hp.hpl.jena.rdf.model.Resource;

public class ImageSemantifier {
	
	private Image mImage;
	private EntitySelector mSelector;

	public ImageSemantifier(Image im, EntitySelector.Type sel) {
		mImage = im;
		mSelector = EntitySelectorFactory.create(sel, im);
	}
	
	public Map<String, Resource> semantify() {
		Map<String, Resource> entities = new HashMap<>();
		List<String> tags = new ArrayList<>(mImage.getTags());
		for (String tag : tags) {
			Resource e = mSelector.select(tag);
			if (e != null) {
				entities.put(tag, e);	
			}
		}
		return entities;
	}
	
	public SemFeatures getFeatures() {
		Map<String, Resource> entitiesMap = semantify();
		List<Resource> entities = new ArrayList<>();
		for (String tag : entitiesMap.keySet()) {
			entities.add(entitiesMap.get(tag));
		}
		try {
			return new SemFeatures(entities);
		} catch (Exception e) {
			// resource consuming. May fail to create the features
			e.printStackTrace();
			return null;
		}
	}

}
