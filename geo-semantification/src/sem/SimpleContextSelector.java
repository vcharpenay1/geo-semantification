package sem;

import ir.RetrievalUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import model.Image;

import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;

/**
 * Builds a set of terms for each entity that will be considered as
 * documents. The context is then considered as a query and classic
 * information retrieval is performed to rank each entity, the most
 * relevant being selected.
 * The IR technique used here is a simple TF-IDF computed for
 * each tag in the context.
 * 
 * The set of terms for one entity is built as follows:
 * each entity (class/property/resource) that appear in a
 * triple where the initial entity also appears are retrieved.
 * Literals are taken as is while URI are processed to get
 * the label that is associated to the entity. If no label is defined,
 * the name that appears in the URI is taken.
 *
 */
public class SimpleContextSelector extends EntitySelector {

	private List<String> mContext;
	
	public SimpleContextSelector(Image im) {
		super(im);
		mContext = im.getTags();
	}
	
	@Override
	public TreeMap<Double, Resource> rank(String tag) {
		List<String> ctx = mContext;
		ctx.remove(tag);
		List<Resource> list = SemanticsManager.getEntities(tag);
		if (list.isEmpty()) {
			return null;
		}
		
		// first index: entity, second index: word in mContext
		List<double[]> tfIdf = new ArrayList<>(list.size());
		// stores how many entities' neighborhoods contain the word
		// in mContext which index is the same as in this list
		int[] counts = new int [ctx.size()];
		for (Resource e : list) {
			List<RDFNode> neighbors = SemanticsManager.getNeighbors(e);
			double[] vector = new double [ctx.size()];
			for (int i = 0; i < ctx.size(); i++) {
				for (RDFNode res : neighbors) {
					String nodeString = (res.isURIResource()) ? (res.asResource().getLocalName()) : (res.toString());
					if (nodeString.contains(ctx.get(i).toLowerCase())) {
						vector[i]++; // computes TF- (from TF-IDF)
					}
				}
				// does the neighborhood contain the current word
				// retrieved from the context ?
				if (vector[i] > 0) {
					counts[i]++; // prepares the computation of -IDF
				}
			}
			tfIdf.add(vector);
		}
		TreeMap<Double, Resource> rankedEntities = new TreeMap<>();
		for (int j = 0; j < tfIdf.size(); j++) {
			for (int i = 0; i < ctx.size(); i++) {
				// -computes -IDF
				if (counts[i] > 0) {
					tfIdf.get(j)[i] *= Math.log10(list.size() / (double) counts[i]);
				} else {
					tfIdf.get(j)[i] = 0;
				}
			}
			// computes the cosine similarity between the vector that represents
			// the query, (1 1 ... 1) for each word in the query, and the vector
			// of the TF-IDF weights for the given entity
			double[] query = new double [ctx.size()];
			for (int k = 0; k < query.length; k++) {
				query[k] = 1.;
			}
			Double cosSim = RetrievalUtil.cosSimilarity(tfIdf.get(j), query);
			if (cosSim.equals(Double.NaN)) {
				System.err.println("error by computing similarity between " + list.get(j).getLocalName() + " and " + ctx);
				RetrievalUtil.cosSimilarity(tfIdf.get(j), query);
			} else {
				rankedEntities.put(cosSim, list.get(j)); // duplicates are erased
			}
		}
		return rankedEntities;
	}
	
}
