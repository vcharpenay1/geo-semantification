package sem;

import ir.RetrievalUtil;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import rpc.FlickrApiManager;
import model.Image;
import model.WikipediaPage;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;

import db.DBConnector;
import db.WikipediaDBManager;

/**
 * See the following article:
 * Tesconi, M., Ronzano, F., Marchetti, A., & Minutoli, S. (2008). Semantify del.icio.us: automatically turn your tags into senses.
 * Accessible at http://citeseerx.ist.psu.edu/viewdoc/summary;jsessionid=7E06AC2D91F268F2AD580D3AEC4CCBCD?doi=10.1.1.142.6371
 *
 */
public class TesconiSelector extends EntitySelector {
	
	protected String mUserId;
	protected WikipediaDBManager mDb;
	protected FlickrApiManager mApi;
	
	/**
	 * 
	 * @param ctx
	 * @param db should be already connected
	 */
	public TesconiSelector(Image im) {
		super(im);
		mUserId = im.getUserId();
		mApi = new FlickrApiManager();
		mDb = DBConnector.getWikipediaDB();
	}
	
	@Override
	public TreeMap<Double, Resource> rank(String tag) {
		try {
			TreeMap<Double, Resource> rankedPages = new TreeMap<>();

			Map<String, Integer> coos = mApi.getCooccuringTags(mUserId, tag);
			if (coos == null || coos.size() == 0) {
				// all pages will have the same value (0), no need to go further
				return null;
			}
			List<WikipediaPage> pages = mDb.getPages(tag, true);
			if (pages.isEmpty()) {
				pages = mDb.getPages(tag, false);
			}
			
			double docMaxCoos = Double.MIN_VALUE; // max |{words}| intersec. |{coos}|
			for (WikipediaPage p : pages) {
				Map<String, Integer> words = p.getWords();
				double senseRank = 0.;
				double docCoos = 0.;
				// number of pages where 'tag' and a tag t co-occur
				Integer nbWordCoo = 0;
				for (String t : coos.keySet()) {
					if (words.containsKey(t)) {
						nbWordCoo++;
						senseRank += coos.get(t) * words.get(t);
						docCoos += words.get(t);
					}
				}
				senseRank *= nbWordCoo / (double) coos.size();
				if (!rankedPages.containsKey(senseRank)) {
					rankedPages.put(senseRank, pageToEntity(p));
					if (rankedPages.lastKey() < senseRank) {
						docMaxCoos = docCoos;
					}
				} else if (rankedPages.lastKey() == senseRank && docCoos > docMaxCoos) {
					// only inserts the page that have the greatest vector norm
					rankedPages.put(senseRank, pageToEntity(p));
					docMaxCoos = docCoos;
				}
			}
			
			return rankedPages;
		} catch (SQLException e) {
			// most likely not connected to the DB
			return null;
		}
	}
	
	protected Resource pageToEntity(WikipediaPage p) {
		// builds an ad-hoc model containing only one entity
		Model model = ModelFactory.createDefaultModel();
		return model.createResource(wikiToDb(p.getUrl()));
	}
	
	/**
	 * Returns a DBpedia URI from a Wikipedia URL.
	 * The mapping is straightforward:
	 * "en.wikipedia.org/wiki/<i>name</i>" <-> "dbpedia.org/resource/<i>name</i>"
	 * @param url
	 * @return
	 */
	protected String wikiToDb(String url) {
		String token = "wikipedia.org/wiki/";
		if (!url.contains(token)) {
			return null;
		}
		// everything after token corresponds to page's name
		String name = url.split(token)[1];
		return "http://dbpedia.org/resource/" + name;
	}

}
