package eval;

import ir.RetrievalUtil;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.Reader;
import java.net.MalformedURLException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;

import model.GeoCoords;
import model.GeoTagging;
import model.Image;
import model.ModelException;
import model.Place;
import model.SemFeatures;
import model.WikipediaPage;

import org.apache.xerces.impl.xpath.XPath.Step;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import rpc.FlickrApiManager;
import sem.AngeletouSelector;
import sem.EntitySelector;
import sem.EntitySelectorFactory;
import sem.GarciaSilvaSelector;
import sem.ImageSemantifier;
import sem.MetaSelector;
import sem.SemanticsManager;
import sem.SimpleContextSelector;
import sem.Stemmer;
import sem.TesconiSelector;
import sem.VerySimpleSelector;

import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.graph.NodeFactory;
import com.hp.hpl.jena.graph.Triple;
import com.hp.hpl.jena.graph.impl.LiteralLabelFactory;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.ResultSetFormatter;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.ResourceFactory;
import com.hp.hpl.jena.sdb.store.FeatureSet;
import com.hp.hpl.jena.sparql.core.Var;
import com.hp.hpl.jena.sparql.syntax.ElementGroup;
import com.hp.hpl.jena.sparql.syntax.ElementTriplesBlock;
import com.hp.hpl.jena.vocabulary.RDFS;

import db.DBConnector;
import db.EvalDBManager;
import db.FlickrDBManager;
import db.WikipediaDBManager;
import db.WikipediaStopWordService;

public class Main {
	
	public static void main(String[] args) {
//		createEvalSet("C:\\Users\\victor\\git\\geo-semantification\\results", 100, 10);
//		splitWikipediaDump("C:\\Users\\victor\\git\\geo-semantification\\wikipedia_dump\\enwiki-20140502-all-titles-in-ns0");
//		createWikipediaIndex("C:\\Users\\victor\\git\\geo-semantification\\wikipedia_dump\\");
		
		SemanticsManager.init("http://lod.openlinksw.com/sparql");
		try {
			WikipediaStopWordService.init("C:\\Users\\victor\\git\\geo-semantification\\stopwords_en.csv");
			DBConnector.initWikipediaDB("localhost", "5432", "GeoSemantification", "postgres", "admin");
			DBConnector.initFlickrDB("localhost", "5432", "GeoSemantification", "postgres", "admin");
			DBConnector.initEvalDB("C:\\Users\\victor\\git\\geo-semantification\\geo-semantification-node\\sem_analysis_db");
		} catch (IOException | SQLException e) {
			e.printStackTrace(); // could not happen, dude
		}

//		applyGeoSemantics("C:\\Users\\victor\\git\\geo-semantification\\results", EntitySelector.Type.spotlight);
//		applySelector("C:\\Users\\victor\\git\\geo-semantification\\results", EntitySelector.Type.angeletou);
		
//		try {
//			Map<Long, GeoTagging> res = DBConnector.getEvalDB().getResults(EntitySelector.Type.spotlight);
//			for (Long id : res.keySet()) {
//				GeoTagging gt = res.get(id);
//				GeoSemantics gs = new GeoSemantics("C:\\Users\\victor\\git\\geo-semantification\\results");
//				if (gt.getLocation() != null) {
//					Double dist = gs.getGeoCoherence(gt.getImage(), gt.getLocation());
//					Double area = gt.getLocation().getArea();
//					System.out.println(gt.getImage().getId() + " " + dist + " " + area);
//				} else {
//					System.out.println(gt.getImage().getId());
//				}
//			}
//		} catch (SQLException | IOException e) {
//			e.printStackTrace();
//		}
		
//		createMLData("C:\\Users\\victor\\git\\geo-semantification\\results_light", EntitySelector.Type.tesconi);
	}
	
	/**
	 * generates <code>nbOfSet</code> files in the folder <code>evalFolder</code>,
	 * which will each contain a set of <code>setLength</code> tags.
	 * @param evalFolder should not be ended by the path separator
	 * @param setLength
	 * @param nbOfSet
	 * @return
	 */
	public static boolean createEvalSet(String evalFolder, int setLength, int nbOfSet) {
		try {
			FlickrDBManager db = DBConnector.getFlickrDB();
			Set<String> globalSet = new HashSet<>();
			File datasetFolder = new File(evalFolder, "dataset");
			datasetFolder.mkdir();
			for (int i = 0; i < nbOfSet; i++) {
				File f = new File(datasetFolder, i + ".txt");
				FileWriter writer;
				try {
					writer = new FileWriter(f);
					// more images are retrieved because of duplicates. TODO not systematical
					Integer count = 0;
					List<Long> ids = db.getImages((long) 10 * setLength, true);
					for (Long id : ids) {
						List<String> tags = db.getImage(id).getTags();
						if (tags.size() > 0) {
							String t = tags.get((int) Math.random() * tags.size());
							if (!globalSet.contains(t)) {
								globalSet.add(t);
								writer.write(t + "@" + id + "\r\n");
								count++;
								if (count >= setLength) {
									break; // no more than setLength tags in the set
								}
							}
						}
					}
					writer.close();
				} catch (IOException e) {
					e.printStackTrace();
					return false;
				}
				// set may have a size lower than setLength. Does not really
				// matter since all sets have to be manually reviewed.
			}
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * Checks that all tags in the evaluation set are unique. If not, return
	 * the list of duplicated tags.
	 * @param datasetFolder
	 * @return
	 */
	public static Set<String> checkUniqueness(String datasetFolder) {
		Set<String> duplicates = new HashSet<>();
		Set<String> tags = new HashSet<>();
		File f = new File(datasetFolder);
		for (File tf : f.listFiles()) {
			try {
				BufferedReader reader = new BufferedReader(new FileReader(tf));
				String tag;
				while ((tag = reader.readLine()) != null) {
					tag = tag.split("@")[0]; // separate tag from the id of the photo it appears in
					if (tags.contains(tag)) {
						duplicates.add(tag);
					} else {
						tags.add(tag);
					}
				}
				reader.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				return null;
			} catch (IOException e) {
				e.printStackTrace();
				return null;
			}
		}
		return duplicates;
	}
	
	/**
	 * Retrieves the size of each subset in the evaluation set.
	 * @param datasetFolder
	 * @return
	 */
	public static List<Integer> getSizes(String datasetFolder) {
		List<Integer> sizes = new ArrayList<>();
		File f = new File(datasetFolder);
		for (File tf : f.listFiles()) {
			try {
				BufferedReader reader = new BufferedReader(new FileReader(tf));
				Integer s = 0;
				while (reader.readLine() != null) {
					s++;
				}
				reader.close();
				sizes.add(s);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				return null;
			} catch (IOException e) {
				e.printStackTrace();
				return null;
			}
		}
		return sizes;
	}
	
	/**
	 * Applies an alternative strategy based on the distribution of the
	 * user's photos on images where no geo-related SWE was found.
	 * @param sel
	 */
	public static void applyUserModel(EntitySelector.Type sel) {
		try {
			FlickrApiManager api = new FlickrApiManager();
			Map<Long, GeoTagging> res = DBConnector.getEvalDB().getResults(sel);
			for (Long id : res.keySet()) {
				if (res.get(id).getLocation() == null) {
					Image im = res.get(id).getImage();
					System.out.println(id + "," + GeoCoords.dist(im.getCoords(), api.getUserModel(im.getUserId(), 0.05)));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static void showGeoCoherency(String resultFile) {
		try {
			GeoSemantics sem = new GeoSemantics("C:\\Users\\victor\\git\\geo-semantification\\results");
			FlickrDBManager fDb = DBConnector.getFlickrDB();
			File f = new File(resultFile);
			BufferedReader reader = new BufferedReader(new FileReader(f));
			String line;
			while ((line = reader.readLine()) != null) {
				String[] split = line.split("@"); // <photo_id>@<swe>
				Long id = Long.parseLong(split[0]);
				String uri = split[1];
				
				Image im = fDb.getImage(id);
				Model model = ModelFactory.createDefaultModel();
				Resource ent = model.createResource(uri);
				Place pl;
				try {
					pl = new Place(ent);
					Double geoCo = sem.getGeoCoherence(im, pl);
					System.out.println(id + " " + geoCo + " " + pl.getArea());
				} catch (ModelException e) {
					// nothing dude
				}
			}
			reader.close();
		} catch (SQLException e) {
			e.printStackTrace();
			// go over the next tag
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return;
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
	}
	
	public static void createMLData(String evalFolder, EntitySelector.Type sel) {
		try {
			FlickrDBManager fDb = DBConnector.getFlickrDB();
			EvalDBManager eDb = DBConnector.getEvalDB();
			Set<Long> alreadyDone = eDb.getFeatures(sel);
			File f = new File(evalFolder, "dataset");
			for (File tf : f.listFiles()) {
				System.out.println("beginning of test file " + tf.getName() + "...");
				BufferedReader reader = new BufferedReader(new FileReader(tf));
				String line;
				while ((line = reader.readLine()) != null) {
					String[] split = line.split("@"); // <tag>@<photo_id>
					Long id = Long.parseLong(split[1]);
					
					if (!alreadyDone.contains(id)) {
						Image im = fDb.getImage(id);
						ImageSemantifier sem = new ImageSemantifier(im, sel);
						SemFeatures feats = sem.getFeatures();
						if (feats != null) {
							eDb.insertFeatures(im, feats, sel);
							System.out.println(feats);
						}
					} else {
						System.out.println("Image " + id + " already processed. Skipped.");
					}
				}
				reader.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			// go over the next tag
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return;
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
	}
	
	public static void applyGeoSemantics(String evalFolder, EntitySelector.Type sel) {
		try {
			FlickrDBManager fDb = DBConnector.getFlickrDB();
			EvalDBManager eDb = DBConnector.getEvalDB();
			Map<Long, GeoTagging> alreadyDone = eDb.getResults(sel);
			File f = new File(evalFolder, "dataset");
			for (File tf : f.listFiles()) {
				System.out.println("beginning of test file " + tf.getName() + "...");
				BufferedReader reader = new BufferedReader(new FileReader(tf));
				String line;
				while ((line = reader.readLine()) != null) {
					String[] split = line.split("@"); // <tag>@<photo_id>
					Long id = Long.parseLong(split[1]);
					
					if (!alreadyDone.containsKey(id) && id != 288697901l) {
						Image im = fDb.getImage(id);
						GeoSemantics sem = new GeoSemantics("C:\\Users\\victor\\git\\geo-semantification\\results");
						Place p = sem.getLocation(im, sel);
						eDb.insertResult(im, p, sel);
						System.out.println(id + "@" + (p != null ? p.getEntity() : null));
					} else {
						System.out.println("Image " + id + " already processed. Skipped.");
					}
				}
				reader.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			// go over the next tag
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return;
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
	}
	
	/**
	 * Reads the evaluation files generated by <code>createEvalSet()</code>
	 * in the given folder and apply the given selector to each term that
	 * it contains.
	 * The results are written in a subfolder which name corresponds to
	 * the given selector's name. Each evaluation file is copied in this
	 * folder and results are appended at the end of each line containing
	 * a tag, formatted as follows : tag@photo_id~entity_URI
	 * @param evalFolder
	 * @param sel
	 */
	public static void applySelector(String evalFolder, EntitySelector.Type sel) {
		try {
			FlickrDBManager fDb = DBConnector.getFlickrDB();
			WikipediaDBManager wDb = DBConnector.getWikipediaDB();
			File f = new File(evalFolder, "dataset");
			File fSel = new File(evalFolder + File.separatorChar + sel);
			if (!fSel.exists()) {
				fSel.mkdir();
			}
			for (File tf : f.listFiles()) {
				File tfSel = new File(fSel, tf.getName());
				if (!tfSel.exists()) {
					System.out.println("beginning of test file " + tf.getName() + "...");
					BufferedReader reader = new BufferedReader(new FileReader(tf));
					FileWriter writer = new FileWriter(tfSel);
					String line;
					while ((line = reader.readLine()) != null) {
						String[] split = line.split("@"); // <tag>@<photo_id>
						String tag = split[0];
						Long id = Long.parseLong(split[1]);
						
						Image im = fDb.getImage(id);
						EntitySelector selector;
						if (sel == EntitySelector.Type.meta) {
							selector = new MetaSelector(im, EntitySelector.Type.tesconi, EntitySelector.Type.angeletou);
						} else {
							selector = EntitySelectorFactory.create(sel, im);
						}
						Resource selected = selector.select(tag);
						if (selected != null) {
							writer.append(line + "~" + selected.getURI() + "\r\n");
							writer.flush();
							System.out.println(selected.getURI() + " has been selected for " + line);
						} else {
							writer.append(line + "~" + "?\r\n");
							writer.flush();
							System.out.println("no entity selected for " + line);
						}
					}
					reader.close();
					writer.close();
				} else {
					System.out.println("test file " + tf.getName() + " already processed. Nothing done.");
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			// go over the next tag
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return;
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
	}
	
	/**
	 * Same as upper, only for a given tag
	 * @param tag
	 * @param id
	 * @param sel
	 */
	public static void applySelector(String tag, Long id, EntitySelector.Type sel) {
		try {
			FlickrDBManager fdb = DBConnector.getFlickrDB();
			WikipediaDBManager wdb = DBConnector.getWikipediaDB();
			
			Image im = fdb.getImage(id);
			EntitySelector selector = EntitySelectorFactory.create(sel, im);
			Resource selected = selector.select(tag);
			if (selected != null) {
				System.out.println(selected.getURI() + " has been selected for " + tag + "@" + id);
			} else {
				System.out.println("no entity selected for " + tag + "@" + id);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Prints the result of the tag cleaning step performed
	 * in {@link AngeletouSelector}.
	 * @param evalFolder
	 */
	public static void showTagCleaning(String evalFolder) {
		try {
			FlickrDBManager fDb = DBConnector.getFlickrDB();
			WikipediaDBManager wDb = DBConnector.getWikipediaDB();
			File f = new File(evalFolder, "dataset");
			for (File tf : f.listFiles()) {
				BufferedReader reader = new BufferedReader(new FileReader(tf));
				String line;
				while ((line = reader.readLine()) != null) {
					String[] split = line.split("@"); // <tag>@<photo_id>
					String tag = split[0];
					Long id = Long.parseLong(split[1]);
					
					Image im = fDb.getImage(id);
					String csv = id + "|";
					for (String t : im.getTags()) {
						csv += t + ",";
					}
					csv = csv.substring(0, csv.length() - 1); // a ',' should be removed
					csv += "|";
					AngeletouSelector selector = new AngeletouSelector(im);
					for (Entry<String, List<String>> e : selector.getCleanedContext().entrySet()) {
						for (String sp : e.getValue()) {
							csv += sp + ",";
						}
					}
					csv = csv.substring(0, csv.length() - 1); // a ',' should be removed
					System.out.println(csv);
				}
				reader.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			// go over the next tag
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return;
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
	}
	
	/**
	 * Prints all the tags in the eval set that do not require disambiguation
	 * (only 1 result returned by our Wikipedia index)
	 * @param evalFolder
	 */
	public static void showUnambiguousTags(String evalFolder) {
		try {
			int counter = 0;
			WikipediaDBManager wDb = DBConnector.getWikipediaDB();
			File f = new File(evalFolder, "dataset");
			for (File tf : f.listFiles()) {
				System.out.println("beginning of test file " + tf.getName() + "...");
				BufferedReader reader = new BufferedReader(new FileReader(tf));
				String line;
				while ((line = reader.readLine()) != null) {
					String[] split = line.split("@"); // <tag>@<photo_id>
					String tag = split[0];
					
					Boolean check = wDb.isAmbiguous(tag);
					if (check == null) {
						System.out.println("no index for tag: " + tag);
					} else if (!check) {
						System.out.println(tag);
						counter++;
					}
				}
				reader.close();
			}
			System.out.println("Count: " + counter);
		} catch (SQLException e) {
			e.printStackTrace();
			// go over the next tag
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return;
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
	}
	
	/**
	 * Computes the frequency of word in the english language
	 * for lengths 0,1,2,3,...
	 * Used in the analysis of the split algorithm in
	 * {@link AngeletouSelector}
	 * @param wordsFile
	 */
	public static void showEnglishWordLengths(String wordsFile) {
		File f = new File(wordsFile);
		try {
			BufferedReader reader = new BufferedReader(new FileReader(f));
			String line;
			Map<Integer, Integer> frequencies = new TreeMap<>();
			while ((line = reader.readLine()) != null) {
				if (line.matches("[\\p{IsLatin}]+|[\\p{IsLatin}]")) {
					if (frequencies.containsKey(line.length())) {
						Integer fOld = frequencies.get(line.length());
						frequencies.put(line.length(), fOld + 1);
					} else {
						frequencies.put(line.length(), 1);
					}
				}
			}
			reader.close();
			System.out.println(frequencies);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Splits a Wikipedia dump (assumed very large) in smaller files,
	 * containing 100.000 words each. The files are located in the same
	 * folder as the original dump file
	 * @param filename
	 */
	public static void splitWikipediaDump(String filename) {
		File f = new File(filename);
		String folder = f.getParent();
		try {
			BufferedReader r = new BufferedReader(new FileReader(f));
			String line;
			int i = 0;
			int fi = 0;
			File splitf = new File(folder, "dump_" + fi);
			FileWriter w = new FileWriter(splitf);
			while ((line = r.readLine()) != null) {
				w.append(line + "\n");
				if (i == 100000) {
					// splits into files containing 100.000 lines (instead of 32M)
					w.close();
					i = 0;
					fi++;
					splitf = new File(folder, "dump_" + fi);
					w = new FileWriter(splitf);
				}
				i++;
			}
			r.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Iterates over all files generated by <code>splitWikipediaDump()</code>
	 * and populates the DB with the corresponding service in {@link WikipediaDBManager}
	 * to create an index of Wikipedia pages from photo tags
	 * @param folderString
	 */
	public static void createWikipediaIndex(String folderString) {
		try {
			WikipediaDBManager db = DBConnector.getWikipediaDB();
			File folder = new File(folderString);
			FilenameFilter filter = new FilenameFilter() {
				@Override
				public boolean accept(File dir, String name) {
					return name.contains("dump_");
				}
			};
			for (File f : folder.listFiles(filter)) {
				System.out.println("creating index for file " + f.getName() + "...");
				db.createIndex(f);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
}
