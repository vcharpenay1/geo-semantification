package eval;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import model.GeoCoords;
import model.Image;
import model.ModelException;
import model.Place;
import sem.EntitySelector;
import sem.EntitySelectorFactory;
import sem.GarciaSilvaSelector;
import sem.ImageSemantifier;
import sem.SemanticsManager;
import sem.TesconiSelector;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Resource;

import db.DBConnector;

public class GeoSemantics {
	
	private class Result {
		public String tag;
		public Image image;
		public Resource entity;
	}
	
	private File mEvalFolder;
	
	public GeoSemantics(String evalFolder) throws IOException {
		mEvalFolder = new File(evalFolder);
		if (!mEvalFolder.isDirectory()) {
			throw new IOException("The given folder is not a directory. Object cannot be created");
		}
	}
	
	/**
	 * Returns the entities that contain geographical information from the
	 * evaluation set for a given method. Currently, only looks whether the
	 * entity is a Place (from DBpedia ontology) or not.
	 * @param selType
	 * @return
	 */
	public Map<Image, Place> getPlaces(EntitySelector.Type selType) {
		Map<Image, Place> geoRes = new HashMap<>();
		
		File fSel = new File(mEvalFolder, selType.name());
		if (!fSel.exists()) {
			System.err.println("No result available for the given method");
			return null;
		}
		
		List<Result> results = parseResultFiles(fSel);
		for (Result r : results) {
			if (Place.isAPlace(r.entity)) {
				Place p;
				try {
					p = new Place(r.entity);
					geoRes.put(r.image, p);
				} catch (ModelException e) {
					// already checked
				}
			}
		}
		
		return geoRes;
	}
	
	/**
	 * Returns the most precise Place entity contained in the
	 * tags of the given image or null if none is found.
	 * @param im
	 * @return
	 */
	public Place getLocation(Image im, EntitySelector.Type selType) {
		ImageSemantifier sem = new ImageSemantifier(im, selType);
		Resource locEnt = null;

		Map<String, Resource> entities = sem.semantify();
		for (String t : entities.keySet()) {
			Resource res = entities.get(t);
			if (Place.isAPlace(res) && (locEnt == null || isMorePrecise(locEnt, res))) {
				locEnt = res;
			}
		}
		
		if (locEnt != null) {
			Place location;
			try {
				location = new Place(locEnt);
				return location;
			} catch (ModelException e) {
				// already checked
			}
		}
		return null;
	}

	/**
	 * Returns an abstract value that defines if an image is coherent with the
	 * resource with which it is associated by giving the distance between their
	 * respective location
	 * TODO right now, returns their distance along the surface of the earth
	 * (47 CFR 73.208 prescription of the FCC)
	 * @param pl
	 * @param ph
	 * @return
	 */
	public Double getGeoCoherence(Image ph, Place pl) {
		GeoCoords pCoords = ph.getCoords();
		GeoCoords rCoords = pl.getCoords();
		if (pCoords == null || rCoords == null) {
			return null;
		}
		
		return GeoCoords.dist(pCoords, rCoords);
	}
	
	/**
	 * Get all results contained in the results files in the form
	 * <i>tag</i>@<i>photo_id</i>~<i>swe_url</i>
	 * @param fSel
	 * @return
	 */
	private List<Result> parseResultFiles(File fSel) {
		List<Result> res = new ArrayList<>();
		
		Model model = ModelFactory.createDefaultModel();
		try {
			for (File tf : fSel.listFiles()) {
				BufferedReader reader = new BufferedReader(new FileReader(tf));
				String line;
				while ((line = reader.readLine()) != null) {
					String[] split = line.split("@");
					String tag = split[0];
					String[] details = split[1].split("~");
					String im = details[0];
					String url = details[1];
					Result r = new Result();
					r.tag = tag;
					r.image = DBConnector.getFlickrDB().getImage(Long.parseLong(im));
					r.entity = model.createResource(url);
					res.add(r);
				}
				reader.close();
			}
		} catch (IOException | NumberFormatException | SQLException e) {
			System.err.println("Cannot retrieve all result from " + fSel.getName());
		}
		return res;
	}
	
	/**
	 * Returns whether entity 1 describes a more precise
	 * place as entity 2 or false if any problem occurs
	 * TODO introduce the Place ontology
	 * @param ent1
	 * @param ent2
	 * @return
	 */
	private Boolean isMorePrecise(Resource ent1, Resource ent2) {
		if (ent1 != null && ent2 != null) {
			Double area1 = SemanticsManager.getArea(ent1);
			Double area2 = SemanticsManager.getArea(ent2);
			if (area1 != null && area2 != null) {
				return area1 < area2;
			}
		}
		return false;
	}
	
}
