package eval;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import model.Image;
import db.DBConnector;
import db.FlickrDBManager;

/**
 * This class formats the photo collection in an XML file having
 * the structure required by the Lemur Toolkit for Information Retrieval.
 * The formatted file is used as input of the following reverse geo-tagging system:
 * https://github.com/chauff/ImageLocationEstimation
 *
 */
public class LemurTkFormatter {

	private int mImTot;
	private int mImPerFile;
	
	/**
	 * Parameterized constructor
	 * @param imagesTotal the number of images to add to the collection
	 * (randomly chosen in the DB)
	 * @param imagesPerFiles the number of images to insert per file
	 */
	public LemurTkFormatter(int imagesTotal, int imagesPerFile) {
		mImTot = imagesTotal;
		mImPerFile = imagesPerFile;
	}
	
	/**
	 * Writes in the folder the different files containing
	 * the formatted collection of images, with the following
	 * metadata:
	 *  - id
	 *  - user id
	 *  - tags
	 *  - title
	 *  - lon/lat
	 * @param folder
	 */
	public void format(File folder) {
		if (!checkFolder(folder)) {
			System.err.println(folder.getPath() + " is no directory. Nothing done.");
			return;
		}
		FlickrDBManager db = DBConnector.getFlickrDB();
		try {
			List<Long> imIds = db.getImages((long) mImTot, true);
			File f = new File (folder, "0.txt");
			FileWriter writer = new FileWriter(f);
			for (int i = 0; i < imIds.size(); i++) {
				if (i > 0 && i % mImPerFile == 0) {
					writer.close();
					System.out.println(f.getName() + " completed.");
					f = new File(folder, Integer.toString(i / mImPerFile) + ".txt");
					writer = new FileWriter(f);
				}
				Image im = db.getImage(imIds.get(i));
				writer.append(toXml(im));
			}
			writer.close();
		} catch (SQLException e) {
			System.err.println("Images could not be retrieved. Nothing done.");
		} catch (IOException e) {
			System.err.println("An IO error occurred: " + e.getMessage());
		}
	}
	
	/**
	 * Similarly to upper method, formats the test set contained
	 * in dataFolder
	 * @param folder
	 * @param dataFolder
	 */
	public void formatTest(File folder, File dataFolder) {
		if (!checkFolder(folder) || !dataFolder.isDirectory()) {
			System.err.println(folder.getPath() + " is no directory. Nothing done.");
			return;
		}
		FlickrDBManager db = DBConnector.getFlickrDB();
		try {
			FileWriter docsWriter = new FileWriter(new File(folder, "test_docs.txt"));
			FileWriter namesWriter = new FileWriter(new File(folder, "test_names.txt"));
			for (File df : dataFolder.listFiles()) {
				BufferedReader reader;
				try {
					reader = new BufferedReader(new FileReader(df));
					String line;
					while ((line = reader.readLine()) != null) {
						String[] split = line.split("@");
						Long id = Long.parseLong(split[1]);
						
						try {
							docsWriter.append(toXml(db.getImage(id)));
							namesWriter.append(id.toString() + "\r\n");
						} catch (SQLException e) {
							System.err.println("Image " + id + " could not be added to the collection.");
						}
					}
					reader.close();
				} catch (IOException e) {
					System.err.println("Cannot retrieve all result from " + dataFolder.getName());
				}
			}
			docsWriter.close();
			namesWriter.close();
		} catch (IOException e1) {
			System.err.println("Could not create formatted file. Abort.");
			return;
		}
	}

	/**
	 * Example:
	 * <DOC>
	 * <DOCNO>2537024282</DOCNO>
	 * <LONGITUDE>-138.51164</LONGITUDE>
	 * <LATITUDE>-89.99999</LATITUDE>
	 * <USERID>10403985@N04</USERID>
	 * <TAGS>de azulooo desfiguraciones</TAGS>
	 * <TITLE>my holidays</TITLE>
	 * <TIMETAKEN>1212154760</TIMETAKEN>
	 * <USERLOCATION>Los Angeles, US, earth</USERLOCATION>
	 * <ACCURACY>6</ACCURACY>
	 * </DOC>
	 * @param im
	 * @param writer
	 */
	private String toXml(Image im) {
		return "<DOC>"
				+ "<DOCNO>" + im.getId() + "</DOCNO>"
				+ "<LONGITUDE>" + im.getCoords().getLon() + "</LONGITUDE>"
				+ "<LATITUDE>" + im.getCoords().getLat() + "</LATITUDE>"
				+ "<USERID>" + im.getUserId() + "</USERID>"
				+ "<TITLE>" + im.getTitle() + "</TITLE>"
				+ "<TAGS>" + toSpaceSeparated(im.getTags()) + "</TAGS>"
			 + "</DOC>" + "\r\n";
	}
	
	private String toSpaceSeparated(List<String> list) {
		String toReturn = "";
		for (int pos = 0; pos < list.size(); pos++) {
			toReturn += list.get(pos);
			if (pos < list.size() - 1) {
				toReturn +=  " ";
			}
		}
		return toReturn;
	}
	
	private Boolean checkFolder(File maybeFolder) {
		if (!maybeFolder.exists()) {
			return maybeFolder.mkdir();
		} else {
			return maybeFolder.isDirectory();
		}
	}
	
}
