package rpc;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import model.GeoCoords;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.winterwell.jgeoplanet.GeoPlanet;
import com.winterwell.jgeoplanet.GeoPlanetException;
import com.winterwell.jgeoplanet.Place;

public class FlickrApiManager {
	
	private static final String GEO_PLANET_APP_ID = "dj0yJmk9VHJDNURIV3I3Y1ZjJmQ9WVdrOVlVdEJZemxHTkdzbWNHbzlNQS0tJnM9Y29uc3VtZXJzZWNyZXQmeD0zZQ--";
	
	private static final String BASE_URL = "https://api.flickr.com/services/rest/?";
	private static final String API_KEY = "64b8d72e502249c8a5132b9c4d3a1560";
	private static final String API_SECRET = "9a3b11b55a44a069";
	
	private static final String PHOTOS_METHOD = "flickr.people.getPublicPhotos";
	private static final String USER_LOC_METHOD = "flickr.people.getInfo";
	
	private GeoPlanet mGeoPlanet;
	
	public FlickrApiManager() {
		try {
			mGeoPlanet = new GeoPlanet(GEO_PLANET_APP_ID);
		} catch (GeoPlanetException e) {
			System.err.println("Could not create Yahoo! Geo Planet client. method getUserLocation will fail.");
		}
	}
	
	public Map<String, Integer> getCooccuringTags(String userId, String tag) {		
		try {
			Map<String, Integer> coos = new HashMap<>();
			Integer page = 1;
			Integer nbPages = 1;
			do {
				Document rsp = Jsoup.connect(formURL(userId, PHOTOS_METHOD, page)).get();
				if (rsp.select("rsp").attr("stat").equals("ok")) {
					// Flickr API answered with a OK status
					nbPages = Integer.parseInt(rsp.select("photos").attr("pages"));
					Elements ps = rsp.select("photo");
					for (Element p : ps) {
						List<String> tags = Arrays.asList(p.attr("tags").split(" "));
						if (tags.contains(tag)) {
							// count co-occurences
							for (String t : tags) {
								if (!t.equals(tag)) {
									if (coos.containsKey(t)) {
										Integer c = coos.get(t);
										coos.put(t, c + 1);
									} else {
										coos.put(t, 1);
									}
								}
									
							}
						}
					}
				} else {
					System.err.println("Flickr API cannot be reached. Abort.");
					// return null;
				}
				page++;
			} while (page < nbPages);
			return coos;
		} catch (IOException e) {
			System.err.println("Flickr API cannot be reached (" + e.getMessage() + ")");
			return null;
		}
	}
	
	public GeoCoords getUserLocation(String userId) {
		Document rsp;
		try {
			rsp = Jsoup.connect(formURL(userId, USER_LOC_METHOD, 1)).get();
			if (rsp.select("rsp").attr("stat").equals("ok")) {
				String loc = rsp.select("location").text();
				Place p = mGeoPlanet.getPlace(loc);
				if (p != null) {
					return new GeoCoords(p.getCentroid().getLongitude(), p.getCentroid().getLatitude());
				}
			} else {
				System.err.println("Flickr API cannot be reached. Abort.");
			}
		} catch (IOException e) {
			System.err.println("Flickr API cannot be reached (" + e.getMessage() + ")");
		} catch (GeoPlanetException e) {
			// complains that the App ID is not correct, but gives the results anyway...
			// e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Computes the most probable location of the user by
	 * retrieving all its photos, binning their location
	 * and picking up the bin with the most photos.
	 * Bins are cells on the map, equally divided in both directions.
	 * @param userId
	 * @param cellSize the cell size in degree. (0.1�, or 0.01,� standard in reverse geo-tagging)
	 * @return
	 */
	public GeoCoords getUserModel(String userId, Double cellSize) {
		try {
			// the key identifies the cell
			Map<Integer, Long> freqs = new HashMap<>();
			Integer page = 1;
			Integer nbPages = 1;
			do {
				Document rsp = Jsoup.connect(formURL(userId, PHOTOS_METHOD, page)).get();
				if (rsp.select("rsp").attr("stat").equals("ok")) {
					// Flickr API answered with a OK status
					nbPages = Integer.parseInt(rsp.select("photos").attr("pages"));
					Elements ps = rsp.select("photo");
					for (Element p : ps) {
						Double lon = null, lat = null; // parsed values
						lat = Double.parseDouble(p.attr("latitude"));
						lon = Double.parseDouble(p.attr("longitude"));
						if (lat != null && lon != null && lat != 0 && lon != 0) {
							// if (0,0), most likely stub values in the API
							Integer cellId = (int) (Math.ceil((lon + 180) / cellSize)
									+ Math.ceil((lat + 90) / cellSize) * (360 / cellSize));
							if (freqs.containsKey(cellId)) {
								Long old = freqs.get(cellId);
								freqs.put(cellId, old + 1);
							} else {
								freqs.put(cellId, 1l);
							}
						}
					}
				} else {
					System.err.println("Flickr API cannot be reached. Abort.");
					// return null;
				}
				page++;
			} while (page < nbPages);
			if (freqs.isEmpty()) {
				return null;
			}
			Integer mostFreq = null;
			for (Integer c : freqs.keySet()) {
				if (mostFreq == null || freqs.get(mostFreq) < freqs.get(c)) {
					mostFreq = c;
				}
			}
			Double meanLon = (mostFreq % (360 / cellSize) + 0.5) * cellSize - 180;
			Double meanLat = (mostFreq / (360 / cellSize) + 0.5) * cellSize - 90;
			return new GeoCoords(meanLon, meanLat);
		} catch (IOException e) {
			System.err.println("Flickr API cannot be reached (" + e.getMessage() + ")");
			return null;
		}
	}
	
	private String formURL(String userId, String method, Integer page) {
		return BASE_URL +
				"method=" + method + "&" +
				"api_key=" + API_KEY + "&" +
				"user_id=" + userId + "&" +
				"page=" + page + "&" +
				"extras=tags,geo";
	}
	
}
