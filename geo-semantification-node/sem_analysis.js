/**
 * File that handles operations related to the semantic
 * analysis of our Master's thesis
 */
var fs = require('fs');
var pg = require('pg');
var sqlite3 = require('sqlite3');

var db = new sqlite3.Database('sem_analysis_db');

const methods = ['baseline', 'tfidf', 'angeletou', 'garcia', 'tesconi'];
var results = [];

/**
 * populates var 'results' by reading
 * the result files contained in
 * folder eval_path.
 * note: synchrone function
 * @param eval_path string representing an absolute path
 */
function init(eval_path, cb) {
	root_folder = eval_path;
	for (m in methods) {
		var mf = eval_path + '\\' + methods[m];
		if (fs.existsSync(mf)) {
			var rf = fs.readdirSync(mf);
			if (rf) {
				for (name in rf) {
					var content = fs.readFileSync(mf + '\\' + rf[name], {encoding: 'utf8'});
					if (content) {
						var images_str = content.split('\r\n');
						images_str.pop(); // last index is an empty string because of split()
						for (i in images_str) {
							parse(images_str[i]);
						}
					}
				}
			}
		}
	}
	persist_results(cb);
}

/**
 * parses the line that should be like 'tag@photo_id~swe_url'
 * and put it in a set of current results that are not
 * persisted yet
 * @param result_line
 * @returns
 */
function parse(result_line) {
	var tag, id, url;
	var split = result_line.split('@');
	tag = split[0];
	split = split[1].split('~');
	id = split[0];
	url = split[1];
	if (results[tag] == undefined) {
		results[tag] = {
				im_id: id,
				swe_url: []
		};
	}
	results[tag].swe_url[methods[m]] = url;
}

/**
 * persists the content of var results in the SQLite
 * database, which has the following schema:
 * results(im_id, tag, swe_url, method, is_ok).
 * @param cb
 */
function persist_results(cb) {
	db.serialize(function() {
		db.run('CREATE TABLE results (im_id INTEGER, tag TEXT, swe_url TEXT, method TEXT, is_ok CHARACTER(1)' +
				', CONSTRAINT pk_results PRIMARY KEY (im_id, tag, method))', function(err) {
			if (err) {
				console.error('error while creating results table: ' + err);
			}
		});
		var statement = db.prepare('INSERT INTO results VALUES (?, ?, ?, ?, NULL)');
		for (var tag in results) {
			var id = results[tag].im_id;
			var urls = results[tag].swe_url;
			for (var method in urls) {
				statement.run(id, tag, urls[method], method, function(err) {
					if (err) {
						console.error('error while inserting tuple: ' + err);
					} else {
						console.log('inserted ' + id + ', ' + tag + ', ' + urls[method] + ' with ' + method + '\'s method');
					}
				});
			}
		}
		statement.finalize();
	});
//	db.close();
	cb();
}

/**
 * populates a second table in the SQLite database
 * with the following schema:
 * split(im_id, tag, cleaned)
 * note: synchrone function
 * @param eval_path
 */
function pop_synt_analysis(res_file) {
	if (fs.existsSync(res_file)) {
		var content = fs.readFileSync(res_file, {encoding: 'utf8'});
		if (content) {
			var images = content.split('\r\n');
			images.pop(); // last index is an empty string because of split()
			db.serialize(function() {
				db.run('CREATE TABLE split (im_id INTEGER, tag TEXT, cleaned BOOLEAN)', function(err) {
					if (err) {
						console.error('error while creating results table: ' + err);
					}
				});
				var statement = db.prepare('INSERT INTO split VALUES (?, ?, ?)');
				for (var im in images) {
					var split = images[im].split('|');
					var id = split[0];
					var tuples = [];
					var original = split[1].split(',');
					for (o in original) {
						tuples.push({
							tag: original[o],
							cleaned: false
						});
					}
					if (split[2]) { // all tags removed for the photo
						var cleaned = split[2].split(',');
						for (c in cleaned) {
							tuples.push({
								tag:cleaned[c],
								cleaned: true
							});
						}
					}
					for (var t in tuples) {
						statement.run(id, tuples[t].tag, tuples[t].cleaned, function(err) {
							if (err) {
								console.error('error while inserting tuple: ' + err);
							} else {
								console.log('inserted ' + id + ', ' + tuples[t].tag + ', ' + tuples[t].cleaned);
							}
						});
					}
				}
				statement.finalize();
			});
		}
	}
}

/**
 * retrieves a result that has not been fully evaluated yet
 * @param cb cb(res), res being an Object which looks like
 * {tag: ..., tag_ctx: ..., im_url: ..., swe_url: ..., eval: ...}
 * or null if a problem occurred
 */
function result(cb) {
	// retrieves the first result that have missing evaluation,
	// takes the tags that were already partially evaluated
	db.all('SELECT * FROM results WHERE tag =' +
			' (SELECT tag FROM results WHERE is_ok IS NULL AND method="angeletou"' + // TODO remove angeletou
			' GROUP BY tag ORDER BY count(tag) LIMIT 1)', function(err, rows) {
		var tag = rows[0].tag; // all rows should contain the same tag
		var res_id = rows[0].im_id; // all rows should contain the same image id
		var res_urls = {};
		var res_eval = {};
		for (var r in rows) {
			// method names in the DB should be the same as in array 'methods'
			res_urls[rows[r].method] = rows[r].swe_url;
			res_eval[rows[r].method] = rows[r].is_ok;
		}
		query_image(res_id, function(im) {
			if (im == null) {
				cb(null);
			} else {
				var r = {
						tag: tag,
						tag_ctx: im.tags,
						im_url: im.url,
						swe_url: res_urls,
						eval: res_eval
				};
				cb(r);
			}
		});
	});
}

/**
 * performs a SQL query to get all information
 * related to the image with the given id.
 * If a DB problem occurs, null is returned
 * in the callback
 * @param id
 * @param cb cb(image)
 */
function query_image(id, cb) {
	var client = new pg.Client('postgres://postgres:admin@localhost/GeoSemantification');
	client.connect(function(err) {
		if (err) {
			console.error('PostgreSQL client cannot connect to the db');
			cb(null);
		} else {
			var q = 'SELECT * FROM photos_new WHERE id=' + id;
			client.query(q, function(err, result) {
				if (err) {
					console.error('photo ' + id + ' cannot be retrieved from the db');
					cb(null);
				} else {
					row = result.rows[0];
					image = {
							id: row.id,
							tags: row.tags,
							url: 'http://farm' + row.flickr_farm + '.staticflickr.com/'
								+ row.flickr_server + '/'
								+ row.id + '_' + row.flickr_secret + '.jpg'
					}
					cb(image);
					client.end();
				}
			});
		}
	});
}

/**
 * updates results DB for a given tags
 * @param tag
 * @param eval an object that has methods values as keys
 * and 'y'/'n'/'?' as values that indicate if a given method
 * performed successfully or not on the given tag, '?'
 * meaning that the evaluator could not determine the answer.
 * @param cb cb(err) err is Boolean
 */
function update(tag, eval, cb) {
	console.log(eval);
	var err_occurred = false;
	db.serialize(function() {
		db.all('SELECT * FROM results WHERE tag = ?', tag, function(err, rows) {
			for (var r in rows) {
				if (!rows[r].is_ok && eval[rows[r].method]) {
					db.run('UPDATE results SET is_ok = ? WHERE tag = ? and method = ?', eval[rows[r].method], tag, rows[r].method, function(err) {
						if (err) {
							console.error('error while updating results table: ' + err);
							err_occurred = true;
						} else {
							console.log('updated tag ' + tag + ' for ' + rows[r].method + '\'s method');
						}
					});
				}
			}
		});
	});
	cb(err_occurred);
}

/**
 * module interface
 */
exports.methods = methods;
exports.init = init;
exports.popsa = pop_synt_analysis;
exports.result = result;
exports.update = update;