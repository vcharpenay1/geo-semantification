/**
 * Main server file
 */
var express = require('express');
var bp = require('body-parser');
var app = express();

var sem = require('./sem_analysis.js');
var geo = require('./placeability.js');

// configures a module that automatically serve data in /public
app.use(express.static(__dirname + '/public'));
// configures a module to populate prop request's body
app.use(bp());
// configures Jade template engine to work with Express
app.set('view engine', 'jade');
app.engine('jade', require('jade').__express);

// sets primary route
app.get('/', function(req, res) {
	res.sendfile(__dirname + '/public/index.html');
});

// sets semantic analysis route
app.get('/gt', function(req, res) {
	sem.result(function(data) {
		if (data == null) {
			res.send(500);
		} else {
			res.render('gt', data);
			console.log('data for tag "' + data.tag + '" served: ' + JSON.stringify(data));
		}
	});
});

app.post('/gt_res', function(req, res) {
	var eval = [];
	for (m in sem.methods) {
		eval[sem.methods[m]] = req.body[sem.methods[m]];
	}
	sem.update(req.body.tag, eval, function(err) {
		if (err) {
			console.error('error while updating evaluating ' + req.body.tag);
		} else {
			console.log('evaluation for tag ' + req.body.tag + ' updated');
		}
		res.redirect('gt');
	});
});

// sets placeability evaluation rout
app.get('/classes', function (req, res) {
	geo.classes(function(c) {
		if (c) {
			res.render('classes', c);
		} else {
			res.send(500);
		}
	});
});

//sem.popsa('C:\\Users\\victor\\Desktop\\syntactic_analysis.txt');

console.log('initializing semantic analysis module...');
// FIXME always tries to rebuild the database before running the server...
sem.init('C:\\Users\\victor\\git\\geo-semantification\\results', function() {
	console.log('semantic analysis module initialized');
	app.listen(8080);
	console.log('server listening on port 8080...');
});