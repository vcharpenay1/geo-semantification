/**
 * File handling browsing and evaluation of
 * the final results of our Placeability
 * classifier
 */
var pg = require('pg');
var sqlite3 = require('sqlite3');

var db = new sqlite3.Database('sem_analysis_db');

/**
 * returns the elements of the 2 classes "placeable"
 * and "not placeable"
 * @param cb cb(classes), classes being
 * an object of the following form:
 * { p: [{swe_url: , im_url: }, ...],
 *   np: [{}, ...]
 * }
 */
function classes(cb) {
	classes = {
		p: [],
		np: []
	};
	var semp, semnp;
	db.serialize(function() {
		db.all('SELECT * FROM eval WHERE NOT swe_url = "?" AND method="tesconi"', function(err, rows) {
			if (err) {
				console.error('error while retrieving the class "placeable": ' + err);
			} else {
				semp = rows.length;
				for (var r in rows) {
					var im;
					query_image(rows[r].im_id, function(im) {
						if (im) {
							classes.p.push({
								swe_url: rows[r].swe_url,
								im_url: im.url,
								im_id: im.id
							});
							semp--;
							if (semp == 0 && semnp == 0) {
								cb(classes);
							}
						} else {
							console.error('error while retrieving image ' + rows[r].im_id);
						}
					});
				}
			}
		});
		db.all('SELECT * FROM eval WHERE swe_url = "?" AND method="tesconi"', function(err, rows) {
			if (err) {
				console.error('error while retrieving the class "not placeable": ' + err);
			} else {
				for (var r in rows) {
					semnp = rows.length;
					query_image(rows[r].im_id, function(im) {
						if (im) {
							classes.np.push({
								swe_url: rows[r].swe_url,
								im_url: im.url
							});
							semnp--;
							if (semp == 0 && semnp == 0) {
								cb(classes);
							}
						} else {
							console.error('error while retrieving image ' + rows[r].im_id);
						}
					});
				}
			}
		});
	});
}

/**
 * performs a SQL query to get all information
 * related to the image with the given id.
 * If a DB problem occurs, null is returned
 * in the callback
 * @param id
 * @param cb cb(image)
 */
function query_image(id, cb) {
	var con = 'postgres://postgres:admin@localhost/GeoSemantification';
	pg.connect(con, function(err, client, done) {
		if (err) {
			console.error('PostgreSQL client cannot connect to the db');
			cb(null);
		} else {
			var q = 'SELECT * FROM photos_new WHERE id=' + id;
			client.query(q, function(err, result) {
				done();
				if (err) {
					console.error('photo ' + id + ' cannot be retrieved from the db');
					cb(null);
				} else {
					row = result.rows[0];
					image = {
							id: row.id,
							tags: row.tags,
							url: 'http://farm' + row.flickr_farm + '.staticflickr.com/'
								+ row.flickr_server + '/'
								+ row.id + '_' + row.flickr_secret + '.jpg'
					}
					cb(image);
				}
			});
		}
	});
}

/**
 * module interface
 */
exports.classes = classes;