Geo-semantification
===================

Overview
--------
Prototype and evaluation setup of our master's thesis,
done between february and september 2014 at the
university of Passau within the frame of a double-degree
with the engineering school INSA Lyon (national institute
of applied sciences).

Thesis reference:  
> Victor Charpenay. Geo-Semantification: Towards Using the
> Semantic Web to Improve Reverse Geo-tagging. Master's
> thesis, 2014. Universität Passau.

Content
-------
- `geo-semantification` contains the Java source code of
our prototype. Libraries are already included.
- `geo-semantification` contains the JS files of a
[node.js](http://nodejs.org/) server used in the
evaluation, along with the database of
all the results of our study (refer to the thesis).
Dependencies are declared in `package.json`.
- `defense` contains the slides of the defense((main file:
`pres.html`). Made with
[reveal.js](http://lab.hakim.se/reveal-js/#/).

Other details can be found in the thesis.

Dependencies
------------
- Java 1.7
- PostgreSQL 9.3
- SQLite 3.7
- node.js 0.1